## 赛题简介

会议签到功能作为会议场景中重要的一个环节，需要拥有便捷性、安全性、高效性等。系统应提供基本的会议管理功能，包括会议申请、参会人管理等，参会人信息可以预先通过人脸或照片进行录入，录入成功后，参会人即可进行人脸识别签到。

链接http://www.cnsoftbei.com/plus/view.php?aid=701

## 本项目实机演示视频地址

【【十一届中国软件杯】 二等奖 演示视频】 

https://www.bilibili.com/video/BV1Lg411U7BL

## 后端部署

需要安装docker-compose，最新版docker中已经包含docker-compose

https://docs.docker.com/engine/install/

执行命令:注意复制纯文本或者手敲

`docker-compose -f docker-compose.yml --compatibility up -d` 

若为新版docker，则命令为:

`docker compose -f docker-compose.yml --compatibility up -d` 



Api文档地址为：IP:8866/swagger-ui.html



完整部署见文档

## 前端项目地址

https://gitee.com/zhq15007040607/meeting-h5



## 

人脸识别部分代码为二开

源项目地址https://gitee.com/open-visual/face-search