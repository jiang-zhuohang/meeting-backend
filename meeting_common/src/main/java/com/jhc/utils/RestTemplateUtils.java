package com.jhc.utils;

import com.alibaba.fastjson.JSONArray;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * @author jiangzhuohang
 * @create 2022/4/8 15:33
 */
public class RestTemplateUtils {

    /**
     * 向目的URL发送post请求
     * @param url       目的url
     * @param params    发送的参数
     * @return  CommonResult
     */
    public static CommonResult sendPostRequest(String url, Object params){
        RestTemplate client = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpMethod method = HttpMethod.POST;
        // 以表单的方式提交
        headers.setContentType(MediaType.parseMediaType("application/json;charset=UTF-8"));
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        headers.add("Accept-Charset", "UTF-8");
        //将请求头部和参数合成一个请求
        Object o = JSONArray.toJSON(params);
        String string = o.toString();
        HttpEntity<String> requestEntity = new HttpEntity<String>(string, headers);
        //执行HTTP请求，将返回的结构使用ResultVO类格式化
        ResponseEntity<CommonResult> response = client.exchange(url, method, requestEntity, CommonResult.class);


        return response.getBody();
    }
}
