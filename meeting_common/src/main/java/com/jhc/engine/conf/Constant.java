package com.jhc.engine.conf;

public class Constant {

    public final static String ParamKeyShardsNum = "shardsNum";
    public final static String ParamKeyMaxDocsPerSegment = "maxDocsPerSegment";

    public final static String ColumnPrimaryKey = "id";
    public final static String ColumnNameFaceId = "face_id";
    public final static String ColumnNameFaceScore = "face_score";
    public final static String ColumnNameFaceIndex = "face_index";
    public final static String ColumnNameFaceVector = "face_vector";
    public final static String ColumnNameSampleId = "sample_id";

}
