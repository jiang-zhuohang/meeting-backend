package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DO.SysAuditLog;

/**
 * <p>
 * 访问日志信息  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-07-02
 */
public interface ISysAuditLogService extends IService<SysAuditLog> {

    /**
     * 错误日志数
     * @return Integer
     */
    Integer logError();

    /**
     * 今日登录数
     * @return Integer
     */
    Integer loginCount();

    /**
     * 今日注册数
     * @return Integer
     */
    Integer registerCount(String type);
}
