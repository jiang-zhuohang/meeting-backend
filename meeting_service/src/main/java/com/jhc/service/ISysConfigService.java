package com.jhc.service;

import com.jhc.DO.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.SysConfigAddDto;
import com.jhc.DTO.SysConfigUpdateDto;
import com.jhc.utils.CommonResult;

/**
 * <p>
 * 签到表 服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
public interface ISysConfigService extends IService<SysConfig> {
    CommonResult getConfig();

    CommonResult addConfig(SysConfigAddDto dto);

    CommonResult updateConfig(SysConfigUpdateDto dto);
}
