package com.jhc.service;

import com.jhc.utils.CommonResult;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 图片 服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-08-06
 */
public interface ICmsImagesService  {


    String fileUpload(MultipartFile file);

    Boolean deleteFile(String path);
}
