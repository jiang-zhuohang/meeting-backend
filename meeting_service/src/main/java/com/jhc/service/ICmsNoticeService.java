package com.jhc.service;

import com.jhc.DO.CmsNotice;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.CmsNoticeAddDto;
import com.jhc.DTO.CmsNoticeDto;
import com.jhc.DTO.CmsNoticeUpdateDto;
import com.jhc.utils.CommonResult;

import java.util.List;

/**
 * <p>
 * 公告 服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-01-08
 */
public interface ICmsNoticeService extends IService<CmsNotice> {
    CommonResult getCmsNotice(CmsNoticeDto dto);

    CommonResult deleteCmsNoticeById(List<Long> list);

    CommonResult addCmsNotice(CmsNoticeAddDto dto);

    CommonResult updateCmsNotice(CmsNoticeUpdateDto updateDto);

    CommonResult getCmsNoticeA();

    CommonResult getCmsNoticeById(Long id);
}
