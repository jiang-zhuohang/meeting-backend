package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.CmsLeave;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.CmsLeaveDto;
import com.jhc.DTO.CmsLeaveAddDto;
import com.jhc.DTO.CmsLeaveDto;
import com.jhc.DTO.CmsLeaveUpdateDto;
import com.jhc.VO.CmsLeaveVo;
import com.jhc.VO.CmsLeaveVo;
import com.jhc.utils.CommonResult;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
public interface ICmsLeaveService extends IService<CmsLeave> {
    IPage<CmsLeaveVo> getCmsLeavePageData(CmsLeaveDto pageVo);

    CommonResult addCmsLeave(CmsLeaveAddDto leaveAddDto);

    CommonResult deleteCmsLeave(List<Long> id);

    CommonResult updateCmsLeave(CmsLeaveUpdateDto updateLeave);

    CommonResult getMeetingUserLeave(String meetingId, String userNumber);
}
