package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.CmsNotice;
import com.jhc.DO.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.*;
import com.jhc.VO.RedisUserInfo;
import com.jhc.VO.SysUserVO;
import com.jhc.VO.UserBaseInfoVo;
import com.jhc.utils.CommonResult;

import java.util.List;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-01-08
 */
public interface ISysUserService extends IService<SysUser> {
    RedisUserInfo login(String number, String password);

    CommonResult register(SysUserRegisterDto registerDto);

    CommonResult updatePersonalPassword(SysUpdatePersonalPasswordDto cmsUpdatePasswordDto);

    Boolean resetPas(List<String> numbers);

    IPage<SysUserVO> getAllUserPageData(SysUserDto allSysUserDto);

    CommonResult getUserInfoByUserNum(String number);

    CommonResult searchUserByRoleId(SearchUserByRoleDto searchDo);

    CommonResult userAddRole(SysUserRoleAddDto cmsUserRoleAdd);

    CommonResult registerAll();

    List<UserBaseInfoVo> getUserListByDepartment(long departmentNumber);


    CommonResult updateUserById(SysUserUpdateDto userUpdateDto);

    CommonResult userRemoveRole(SysUserRoleAddDto cmsUserRoleAdd);
}
