package com.jhc.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.CmsAttendance;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.CmsAttendanceAddDto;
import com.jhc.DTO.CmsAttendanceDto;
import com.jhc.DTO.CmsAttendanceUpdateDto;
import com.jhc.VO.CmsAttendanceVo;
import com.jhc.utils.CommonResult;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
public interface ICmsAttendanceService extends IService<CmsAttendance> {
    IPage<CmsAttendanceVo> getCmsAttendancePageData(CmsAttendanceDto pageVo);

    CommonResult addCmsAttendance(CmsAttendanceAddDto signInAddDto);

    CommonResult deleteCmsAttendance(List<Long> id);

    CommonResult updateCmsAttendance(CmsAttendanceUpdateDto updateAttendance);

}
