package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.CmsStaff;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.CmsStaffAddDto;
import com.jhc.DTO.CmsStaffDto;
import com.jhc.DTO.CmsStaffUpdateDto;
import com.jhc.VO.CmsStaffVo;
import com.jhc.utils.CommonResult;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
public interface ICmsStaffService extends IService<CmsStaff> {
    IPage<CmsStaffVo> getCmsStaffPageData(CmsStaffDto pageVo);

    CommonResult addCmsStaff(CmsStaffAddDto staffAddDto);

    CommonResult deleteCmsStaff(List<Long> id);

    CommonResult updateCmsStaff(CmsStaffUpdateDto updateStaff);

    List<Map<String,Object>> getStaffList();
}
