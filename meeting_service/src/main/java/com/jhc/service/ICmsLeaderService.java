package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.CmsLeader;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.CmsLeaderAddDto;
import com.jhc.DTO.CmsLeaderDto;
import com.jhc.DTO.CmsLeaderUpdateDto;
import com.jhc.VO.CmsLeaderVo;
import com.jhc.utils.CommonResult;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
public interface ICmsLeaderService extends IService<CmsLeader> {

    IPage<CmsLeaderVo> getCmsLeaderPageData(CmsLeaderDto pageVo);

    CommonResult addCmsLeader(CmsLeaderAddDto leaderAddDto);

    CommonResult deleteCmsLeader(List<Long> id);

    CommonResult updateCmsLeader(CmsLeaderUpdateDto updateLeader);

    List<Map<String,Object>> getLeaderList();
}
