package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.CmsRoom;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.CmsRoomAddDto;
import com.jhc.DTO.CmsRoomDto;
import com.jhc.DTO.CmsRoomUpdateDto;
import com.jhc.VO.CmsRoomVo;
import com.jhc.utils.CommonResult;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
public interface ICmsRoomService extends IService<CmsRoom> {
    IPage<CmsRoomVo> getCmsRoomPageData(CmsRoomDto pageVo);

    CommonResult addCmsRoom(CmsRoomAddDto roomAddDto);

    CommonResult deleteCmsRoom(List<Long> id);

    CommonResult updateCmsRoom(CmsRoomUpdateDto updateRoom);


    List<Map<String,Object>> getRoomsByStatus(Long status);
}
