package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DO.SysRole;
import com.jhc.DTO.SysAddUserRoleDto;
import com.jhc.DTO.SysRoleAddDto;
import com.jhc.DTO.SysRoleUpdateDto;

/**
 * <p>
 * cms_role  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-07-02
 */
public interface ISysRoleService extends IService<SysRole> {

    boolean addRole(SysRoleAddDto sysRoleAddDto);

    boolean updateRole(SysRoleUpdateDto sysRoleUpdateDto);

    boolean addUserRole(SysAddUserRoleDto addUserRoleDto);

}
