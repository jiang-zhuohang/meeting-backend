package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.CmsMeeting;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.CmsMeetingAddDto;
import com.jhc.DTO.CmsMeetingUpdateDto;
import com.jhc.DTO.CmsMeetingDto;
import com.jhc.DTO.ManualSignDto;
import com.jhc.VO.CmsMeetingVo;
import com.jhc.VO.MeetingInfoVo;
import com.jhc.utils.CommonResult;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
public interface ICmsMeetingService extends IService<CmsMeeting> {

    IPage<CmsMeetingVo> getCmsMeetingPageData(CmsMeetingDto pageVo);

    CommonResult getUserInitiatedMeetingList(String userNumber,String status);

    CommonResult getUserJoinMeetingList(String userNumber,String status);

    CommonResult addCmsMeeting(CmsMeetingAddDto meetingAddDto);

    CommonResult deleteCmsMeeting(List<Long> id);

    CommonResult updateCmsMeeting(CmsMeetingUpdateDto updateMeeting);

    MeetingInfoVo getCmsMeetingById(Long id);

    CommonResult getCmsMeetingList(String status);

    CommonResult manualSign(ManualSignDto manualSignDto);

    CommonResult endMeeting(Long meetingId);

    CommonResult push();

    Boolean ifApproval();
}
