package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.CmsDepartment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DO.SysUser;
import com.jhc.DTO.CmsDepartmentAddDto;
import com.jhc.DTO.CmsDepartmentDto;
import com.jhc.DTO.CmsDepartmentUpdateDto;
import com.jhc.VO.CmsDepartmentVo;
import com.jhc.DTO.SysUserDto;
import com.jhc.VO.UserBaseInfoVo;
import com.jhc.utils.CommonResult;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
public interface ICmsDepartmentService extends IService<CmsDepartment> {
    IPage<CmsDepartmentVo> getCmsDepartmentPageData(CmsDepartmentDto pageVo);

    CommonResult addCmsDepartment(CmsDepartmentAddDto departmentAddDto);

    CommonResult deleteCmsDepartment(List<Long> id);

    CommonResult updateCmsDepartment(CmsDepartmentUpdateDto updateDepartment);

    List<Map<String,Object>> getDepartmentList();

    List<Map<String,Object>> getBasDepartmentList();

    Map<String,List<UserBaseInfoVo>> getDepartmentUserList();
}
