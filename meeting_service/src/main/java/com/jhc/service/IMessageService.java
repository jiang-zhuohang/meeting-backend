package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.Message;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.DTO.*;
import com.jhc.VO.MessagePageVo;
import com.jhc.VO.MessageVO;
import com.jhc.utils.CommonResult;

import java.util.List;

/**
 * <p>
 * 消息表 服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-07
 */
public interface IMessageService extends IService<Message> {

    CommonResult addMessage(MessageAddDto messageAddDto);

    CommonResult addFormatMessage(MessageFormatAddDto dto);

    boolean deleteMessageById(List<Long> list);

    boolean updateMessageById(MessageUpdateDto messageUpdateDto);

    IPage<MessageVO> getMessagePageData(MessageDto pageVo);

    IPage<MessagePageVo> getUserMessageData(MessagePageDto dto);

    CommonResult updateIsRead(List<MessageUpdateIsReadDto> dtoList);

    CommonResult push();
}
