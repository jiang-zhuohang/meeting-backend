package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsLeader;
import com.jhc.DTO.CmsLeaderAddDto;
import com.jhc.DTO.CmsLeaderDto;
import com.jhc.DTO.CmsLeaderUpdateDto;
import com.jhc.VO.CmsLeaderVo;
import com.jhc.mapper.CmsLeaderMapper;
import com.jhc.service.ICmsLeaderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@Service
public class CmsLeaderServiceImpl extends ServiceImpl<CmsLeaderMapper, CmsLeader> implements ICmsLeaderService {

    @Autowired
    private CmsLeaderMapper cmsLeaderMapper;

    @Override
    public IPage<CmsLeaderVo> getCmsLeaderPageData(CmsLeaderDto pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        IPage<CmsLeaderVo>  result =  cmsLeaderMapper.getLeaderPageData(page,pageVo);
        return result;
    }

    @Override
    public CommonResult addCmsLeader(CmsLeaderAddDto leaderAddDto) {
        CmsLeader cmsLeader = new CmsLeader();
        BeanUtil.copyProperties(leaderAddDto,cmsLeader);
        int result =  cmsLeaderMapper.insert(cmsLeader);
        if(result >=1){
            return CommonResult.success("新增成功!");
        }else {
            return CommonResult.failed("新增失败！");
        }
    }

    @Override
    public CommonResult deleteCmsLeader(List<Long> id) {
        int result =   cmsLeaderMapper.deleteBatchIds(id);
        if(result >=1){
            return CommonResult.success("删除成功!");
        }else {
            return CommonResult.failed("删除失败！");
        }
    }

    @Override
    public CommonResult updateCmsLeader(CmsLeaderUpdateDto updateLeader) {
        CmsLeader cmsLeader = new CmsLeader();
        BeanUtil.copyProperties(updateLeader,cmsLeader);
        int result =  cmsLeaderMapper.updateById(cmsLeader);
        if(result >=1){
            return CommonResult.success("修改成功!");
        }else {
            return CommonResult.failed("修改失败！");
        }
    }

    @Override
    public List<Map<String, Object>> getLeaderList() {
        List<Map<String ,Object>> resultList = new ArrayList<>();
        List<CmsLeader> bacLeaders = baseMapper.selectList(null);
        bacLeaders.stream().forEach(
                leader->{
                    Map<String, Object> cmsLeader = new HashMap<>();
                    cmsLeader.put("userNumber",leader.getNumber());
                    cmsLeader.put("userName",leader.getName());
                    resultList.add(cmsLeader);
                }
        );
        return resultList;
    }
}
