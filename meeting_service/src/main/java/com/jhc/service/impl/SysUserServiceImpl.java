package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.BO.AdminAuthData;
import com.jhc.DO.CmsLeader;
import com.jhc.DO.CmsStaff;
import com.jhc.DO.SysRole;
import com.jhc.DO.SysUser;
import com.jhc.DTO.*;
import com.jhc.VO.RedisUserInfo;
import com.jhc.VO.SysUserSearchVO;
import com.jhc.VO.SysUserVO;
import com.jhc.VO.UserBaseInfoVo;
import com.jhc.mapper.CmsLeaderMapper;
import com.jhc.mapper.CmsStaffMapper;
import com.jhc.mapper.SysRoleMapper;
import com.jhc.mapper.SysUserMapper;
import com.jhc.service.ISysPermissionService;
import com.jhc.service.ISysUserService;
import com.jhc.service.RedisService;
import com.jhc.utils.CommonResult;
import com.jhc.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-01-08
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Value("${jwt.expiration}")
    private Long redisTime;
    private static final Logger LOGGER = LoggerFactory.getLogger(SysUser.class);
    private static final String CACHE_PUNCH_REGION = "USER";

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ISysPermissionService permissionService;
    @Autowired
    private SysRoleMapper roleMapper;
    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private RedisService redisService;
    @Autowired
    private CmsLeaderMapper cmsLeaderMapper;
    @Autowired
    private CmsStaffMapper cmsStaffMapper;

    @Override
    public RedisUserInfo login(String number, String password) {
        String token = null;
        RedisUserInfo redisUserInfo = new RedisUserInfo();
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(number);
            if (!passwordEncoder.matches(password, userDetails.getPassword())) {
                throw new BadCredentialsException("密码不正确");
            }
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            token = jwtUtils.generateToken(userDetails);

            // 获取用户所携带的按钮信息  0代表权限数据 1代表返回权限个数

            List<String> permissionValue = permissionService.getPermission(number, "0");
            redisUserInfo.setNumber(number);
            //查询用户信息
            SysUser user = userMapper.selectOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getNumber, number));
            redisUserInfo.setId(user.getId());
            redisUserInfo.setName(user.getName());
            if (ObjectUtil.isNotEmpty(permissionValue)){
                redisUserInfo.setPermissionValueList(permissionValue);
            }
            redisUserInfo.setToken(tokenHead + token);
            redisUserInfo.setType(user.getType());
            redisService.set(CACHE_PUNCH_REGION, number, JSON.toJSONString(redisUserInfo),redisTime);  //保存到redis中
//            redisService.expire(number, redisTime);
        } catch (Exception e) {
            LOGGER.warn("登陆异常:{}", e.getMessage());
        }
        return redisUserInfo;
    }
    @Override
    public CommonResult registerAll() {
        List<SysUserRegisterDto> sysUserRegisterDtos = new ArrayList<>();
        List<CmsStaff> basStaffs = cmsStaffMapper.selectList(new QueryWrapper<CmsStaff>().lambda().eq(CmsStaff::getIsEnable, 1));
        List<CmsLeader> basLeaders = cmsLeaderMapper.selectList(null);
        for (CmsLeader basLeader : basLeaders) {
            SysUserRegisterDto sysUserRegisterDto = new SysUserRegisterDto();
            sysUserRegisterDto.setName(basLeader.getName());
            sysUserRegisterDto.setNumber(basLeader.getNumber());
            sysUserRegisterDto.setSex(basLeader.getSex());
            sysUserRegisterDto.setType(20);
            sysUserRegisterDto.setPassword("000000");
            sysUserRegisterDto.setDepartmentNumber(basLeader.getDepartmentNumber());
            sysUserRegisterDtos.add(sysUserRegisterDto);
        }
        for (CmsStaff basStaff : basStaffs) {
            SysUserRegisterDto sysUserRegisterDto = new SysUserRegisterDto();
            sysUserRegisterDto.setName(basStaff.getName());
            sysUserRegisterDto.setNumber(basStaff.getNumber());
            sysUserRegisterDto.setSex(basStaff.getSex());
            sysUserRegisterDto.setType(10);
            sysUserRegisterDto.setPassword("000000");
            sysUserRegisterDto.setDepartmentNumber(basStaff.getDepartmentNumber());
            sysUserRegisterDtos.add(sysUserRegisterDto);
        }
        for (SysUserRegisterDto sysUserRegisterDto : sysUserRegisterDtos) {
            this.register(sysUserRegisterDto);
        }

        return null;
    }

    @Override
    public List<UserBaseInfoVo> getUserListByDepartment(long departmentNumber) {
        List<SysUser> sysUsers = userMapper.selectList(new QueryWrapper<SysUser>().lambda().eq(SysUser::getDepartmentNumber, departmentNumber));
        List<UserBaseInfoVo> users = new ArrayList<>();
        if (ObjectUtil.isEmpty(sysUsers)){
            return null;
        }
        for (SysUser sysUser : sysUsers){
            UserBaseInfoVo userBaseInfoVo = new UserBaseInfoVo();
            BeanUtil.copyProperties(sysUser,userBaseInfoVo);
            users.add(userBaseInfoVo);
        }
        return users;
    }

    @Override
    public CommonResult updateUserById(SysUserUpdateDto userUpdateDto) {
        SysUser sysUser = new SysUser();
        BeanUtil.copyProperties(userUpdateDto,sysUser);
        int result = userMapper.updateById(sysUser);
        if (result >= 1){
            return CommonResult.success("更新成功");
        }
        return CommonResult.failed("更新失败");
    }

    @Override
    public CommonResult register(SysUserRegisterDto registerDto) {
        if (StringUtils.isEmpty(registerDto.getPassword())) {
            registerDto.setPassword("000000");  //默认注册密码
        }
        SysUser userInfo = userMapper.selectOne(new QueryWrapper<SysUser>().lambda()
                .eq(SysUser::getNumber, registerDto.getNumber()));

        if (ObjectUtil.isNull(userInfo)) {
            SysRole sysRole = null;
            SysUser sysUser = new SysUser();
            BeanUtils.copyProperties(registerDto, sysUser);
//            优化判定
            CmsLeader leaderInfo = cmsLeaderMapper.selectOne(new QueryWrapper<CmsLeader>().lambda()
                        .eq(CmsLeader::getNumber, sysUser.getNumber()));

            if (ObjectUtil.isNotEmpty(leaderInfo)) { //领导层
                if (!ObjectUtil.equal(leaderInfo.getName(), sysUser.getName()))
                    return CommonResult.failed("姓名与编号不匹配");

                sysUser.setPhone(leaderInfo.getPhone());
                sysRole = roleMapper.selectOne(new QueryWrapper<SysRole>().lambda().eq(SysRole::getName, "领导层"));//获取普通员工权限
                sysUser.setType(20);
                sysUser.setDepartmentNumber(leaderInfo.getDepartmentNumber());
            } else {
                List<CmsStaff> cmsStaffs = cmsStaffMapper.selectList(new QueryWrapper<CmsStaff>().lambda()
                        .eq(CmsStaff::getNumber, sysUser.getNumber()).eq(CmsStaff::getIsEnable, 1));
                if (ObjectUtil.isEmpty(cmsStaffs)) {
                    return CommonResult.failed("数据库中无该编号成员");
                }
                CmsStaff staffInfo = cmsStaffs.get(0);
                if (!ObjectUtil.equal(staffInfo.getName(), sysUser.getName()))
                    return CommonResult.failed("姓名与编号不匹配");

                sysUser.setPhone(staffInfo.getPhone());
                sysRole = roleMapper.selectOne(new QueryWrapper<SysRole>().lambda().eq(SysRole::getName, "普通员工"));//获取普通员工权限
                sysUser.setType(10);
                sysUser.setDepartmentNumber(staffInfo.getDepartmentNumber());
            }
            //设置权限
            //将对象转为字符串

            List<String> roleId = new ArrayList<>();
            roleId.add(sysRole.getId().toString());
            AdminAuthData adminAuthData = new AdminAuthData();
            adminAuthData.setAuthList(roleId);
            sysUser.setAuthData(JSONUtil.toJsonStr(adminAuthData));
            sysUser.setPassword(passwordEncoder.encode(sysUser.getPassword()));
            sysUser.setSex(registerDto.getSex());
            int result = userMapper.insert(sysUser);
            if (result == 1) {
                return CommonResult.success("注册成功");
            }
        }
        return CommonResult.failed("注册失败，该账号已存在");

    }

    @Override
    public CommonResult updatePersonalPassword(SysUpdatePersonalPasswordDto cmsUpdatePasswordDto) {
        SysUser user = userMapper.selectOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getNumber, cmsUpdatePasswordDto.getNumber()));
        if (user == null) {
            return CommonResult.failed("用户不存在");
        }
        if (!passwordEncoder.matches(cmsUpdatePasswordDto.getOldPassword(), user.getPassword())) {
            return CommonResult.failed("原密码错误");
        }
        if (cmsUpdatePasswordDto.getNewPassword().length() < 6) {
            return CommonResult.failed("密码长度应大于等于6");
        }
        user.setPassword(passwordEncoder.encode(cmsUpdatePasswordDto.getNewPassword()));
        int update = userMapper.updateById(user);
        if (update == 1) {
            redisService.remove(user.getNumber()); //移除redis缓存
        }
        if (update == 0) {
            return CommonResult.failed("密码修改失败");
        }
        return CommonResult.success("修改成功");
    }

    @Override
    public Boolean resetPas(List<String> numbers) {
        List<SysUser> users = userMapper.selectList(new QueryWrapper<SysUser>().lambda()
                .in(SysUser::getNumber, numbers));
        if (users.size() != numbers.size()) {
            return false;
        }
        for (SysUser user : users) {
            user.setPassword(passwordEncoder.encode("000000"));
            int update = userMapper.updateById(user);
            if (update == 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public IPage<SysUserVO> getAllUserPageData(SysUserDto allSysUserDto) {
        Page page = new Page(allSysUserDto.getCurrentPage(), allSysUserDto.getPageSize());
        IPage<SysUserVO> result = userMapper.getAllUserPageData(page, allSysUserDto);
        return result;
    }

    @Override
    public CommonResult getUserInfoByUserNum(String number) {
        SysUser sysUser = userMapper.selectOne(new QueryWrapper<SysUser>().lambda()
                .eq(SysUser::getNumber, number));
        if (ObjectUtil.isNotEmpty(sysUser)) {
            return CommonResult.success(sysUser);
        }

        return null;
    }

    @Override
    public CommonResult searchUserByRoleId(SearchUserByRoleDto searchDo) {
        List<SysUserSearchVO> result = userMapper.searchUserByRoleId(searchDo);
        return CommonResult.success(result);
    }

    @Override
    public CommonResult userAddRole(SysUserRoleAddDto sysUserRoleAdd) {
        String roleId = sysUserRoleAdd.getRoleId();
        List<Long> idLists = sysUserRoleAdd.getIdList(); //用户id
        int count = 0;
        for (Long idList : idLists) { //循环用户id
            SysUser sysUser = userMapper.selectOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getId, idList));
            AdminAuthData userAuthData = new AdminAuthData();
            AdminAuthData origAuthData = JSONUtil.toBean(sysUser.getAuthData(), AdminAuthData.class); //原始权限
            List<String> origAuthList = origAuthData.getAuthList(); //原始权限
            origAuthList.add(roleId); //加入新的权限
            List<String> collect = origAuthList.stream().distinct().collect(Collectors.toList()); //去重
            userAuthData.setAuthList(collect);
            sysUser.setAuthData(JSONUtil.toJsonStr(userAuthData)); //设置用户拥有指定角色权限
            int i = userMapper.updateById(sysUser);
            if (i == 1) {
                redisService.remove(sysUser.getNumber());
                count = count + 1;
            } else if (i != 1) {
                return CommonResult.failed("更改失败，找管理员！");
            }
        }
        return CommonResult.success("权限赋予成功共赋予 " + count + " 个人");

    }


    @Override
    public CommonResult userRemoveRole(SysUserRoleAddDto sysUserRoleAdd) {
        String roleId = sysUserRoleAdd.getRoleId();
        List<Long> idLists = sysUserRoleAdd.getIdList();
        int count = 0;
        for (Long idList : idLists) {
            SysUser sysUser = userMapper.selectOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getId, idList));
            AdminAuthData userAuthData = new AdminAuthData();
            AdminAuthData adminAuthData = JSONUtil.toBean(sysUser.getAuthData(), AdminAuthData.class);
            List<String> result = new ArrayList<>();
            List<String> authLists = adminAuthData.getAuthList();
            for (String list : authLists) {
                if (!list.equals(roleId)) {
                    result.add(list);
                }
            }
            List<String> result2 = result.stream().distinct().collect(Collectors.toList()); //去重
            userAuthData.setAuthList(result2);
            sysUser.setAuthData(JSONUtil.toJsonStr(userAuthData)); //设置用户拥有指定角色权限
            int i = userMapper.updateById(sysUser);
            if (i == 1) {
                redisService.remove(sysUser.getNumber());
                count = count + 1;
            } else if (i != 1) {
                return CommonResult.failed("更改失败，找管理员！");
            }
        }
        return CommonResult.success("权限去除成功共删除 " + count + " 个人");
    }
}
