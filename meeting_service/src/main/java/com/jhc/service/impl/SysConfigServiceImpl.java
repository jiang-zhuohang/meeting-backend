package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.jhc.DO.SysConfig;
import com.jhc.DTO.SysConfigAddDto;
import com.jhc.DTO.SysConfigUpdateDto;
import com.jhc.mapper.SysConfigMapper;
import com.jhc.service.ISysConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 签到表 服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

    @Autowired
    private SysConfigMapper sysConfigMapper;

    @Override
    public CommonResult getConfig() {
        SysConfig sysConfig = sysConfigMapper.selectOne(null);
        return CommonResult.success(sysConfig);
    }

    @Override
    public CommonResult addConfig(SysConfigAddDto dto) {
        List<SysConfig> sysConfigs = sysConfigMapper.selectList(null);
        if (ObjectUtil.isNotEmpty(sysConfigs)){
            return CommonResult.failed("已有签到配置数据，不可新增");
        }
        SysConfig sysConfig = new SysConfig();
        BeanUtil.copyProperties(dto, sysConfig);
        int result = sysConfigMapper.insert(sysConfig);
        if (result >= 1) {
            return CommonResult.success("新增成功!");
        } else {
            return CommonResult.failed("新增失败！");
        }
    }

    @Override
    public CommonResult updateConfig(SysConfigUpdateDto dto) {
        SysConfig sysConfig = new SysConfig();
        BeanUtil.copyProperties(dto, sysConfig);
        int result = sysConfigMapper.updateById(sysConfig);
        if (result >= 1) {
            return CommonResult.success("更新成功!");
        } else {
            return CommonResult.failed("更新失败！");
        }
    }


}
