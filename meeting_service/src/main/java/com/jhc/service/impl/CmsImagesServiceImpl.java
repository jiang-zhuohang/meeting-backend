package com.jhc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.jhc.service.ICmsImagesService;
import com.jhc.utils.CommonResult;
import com.jhc.utils.FileNameUtil;
import com.jhc.utils.FileUploadUtil;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 图片 服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-08-06
 */
@Service
public class CmsImagesServiceImpl implements ICmsImagesService {
    @Value("${web.images}")
    private String localPath;




    @Override
    public String fileUpload(MultipartFile file) {
        if (file == null){
            return null;
        }
        //获取新文件名
        String fileName = file.getOriginalFilename();

        fileName = FileNameUtil.getFileName(fileName);
        //拼接文件路径，上传到服务器
        if (FileUploadUtil.upload(file,localPath,fileName)){
//            String attachment =mappingPath+"/"+fileName;
//            return attachment;
            return fileName;
        }
        return "上传失败";
    }

    public Boolean deleteFile(String path){
        return FileUploadUtil.delete(localPath+path);
    }


}
