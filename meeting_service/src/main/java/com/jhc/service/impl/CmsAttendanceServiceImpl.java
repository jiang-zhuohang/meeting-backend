package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsAttendance;
import com.jhc.DO.CmsAttendance;
import com.jhc.DTO.*;
import com.jhc.VO.CmsAttendanceVo;
import com.jhc.VO.CmsAttendanceVo;
import com.jhc.VO.MeetingInfoVo;
import com.jhc.mapper.CmsAttendanceMapper;
import com.jhc.service.ICmsAttendanceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.service.ICmsMeetingService;
import com.jhc.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
@Service
public class CmsAttendanceServiceImpl extends ServiceImpl<CmsAttendanceMapper, CmsAttendance> implements ICmsAttendanceService {

    @Autowired
    private CmsAttendanceMapper cmsAttendanceMapper;

    @Autowired
    private ICmsMeetingService cmsMeetingService;
    
    
    @Override
    public IPage<CmsAttendanceVo> getCmsAttendancePageData(CmsAttendanceDto pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        IPage<CmsAttendanceVo> result = cmsAttendanceMapper.getCmsAttendancePage(page, pageVo);
        return result;
    }

    @Override
    public CommonResult addCmsAttendance(CmsAttendanceAddDto signInAddDto) {

        CmsAttendance cmsAttendance = new CmsAttendance();
        BeanUtil.copyProperties(signInAddDto, cmsAttendance);
        int result = cmsAttendanceMapper.insert(cmsAttendance);
        if (result >= 1) {
            return CommonResult.success("新增成功!");
        } else {
            return CommonResult.failed("新增失败！");
        }


    }

    @Override
    public CommonResult deleteCmsAttendance(List<Long> list) {
        int result = cmsAttendanceMapper.deleteBatchIds(list);
        if (result >= 1) {
            return CommonResult.success("删除成功!");
        } else {
            return CommonResult.failed("删除失败！");
        }
    }

    @Override
    public CommonResult updateCmsAttendance(CmsAttendanceUpdateDto updateAttendance) {
        CmsAttendance cmsAttendance = new CmsAttendance();
        BeanUtil.copyProperties(updateAttendance, cmsAttendance);
        int result = cmsAttendanceMapper.updateById(cmsAttendance);
        if (result >= 1) {
            return CommonResult.success("修改成功!");
        } else {
            return CommonResult.failed("修改失败！");
        }
    }


}
