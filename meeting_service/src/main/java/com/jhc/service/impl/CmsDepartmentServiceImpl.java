package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsDepartment;
import com.jhc.DO.CmsDepartment;
import com.jhc.DO.SysUser;
import com.jhc.DTO.CmsDepartmentAddDto;
import com.jhc.DTO.CmsDepartmentDto;
import com.jhc.DTO.CmsDepartmentUpdateDto;
import com.jhc.VO.CmsDepartmentVo;
import com.jhc.VO.UserBaseInfoVo;
import com.jhc.mapper.CmsDepartmentMapper;
import com.jhc.mapper.SysUserMapper;
import com.jhc.service.ICmsDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.utils.CommonResult;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
@Service
public class CmsDepartmentServiceImpl extends ServiceImpl<CmsDepartmentMapper, CmsDepartment> implements ICmsDepartmentService {
    @Autowired
    private CmsDepartmentMapper cmsDepartmentMapper;

    @Autowired
    private SysUserMapper userMapper;
    @Override
    public IPage<CmsDepartmentVo> getCmsDepartmentPageData(CmsDepartmentDto pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        IPage<CmsDepartmentVo>  result =  cmsDepartmentMapper.getDepartmentPageData(page,pageVo);
        return result;
    }

    @Override
    public CommonResult addCmsDepartment(CmsDepartmentAddDto departmentAddDto) {
        SysUser sysUser = userMapper.selectOne(new QueryWrapper<SysUser>().lambda()
                .eq(SysUser::getNumber, departmentAddDto.getUserNumber())
                .eq(SysUser::getName,departmentAddDto.getUserName()));
        if (ObjectUtil.isEmpty(sysUser)){
            return CommonResult.failed("未找到该负责人用户");
        }
        CmsDepartment cmsDepartment = new CmsDepartment();
        BeanUtil.copyProperties(departmentAddDto,cmsDepartment);
        int result =  cmsDepartmentMapper.insert(cmsDepartment);
        if(result >=1){
            return CommonResult.success("新增成功!");
        }else {
            return CommonResult.failed("新增失败！");
        }
    }

    @Override
    public CommonResult deleteCmsDepartment(List<Long> id) {
        int result =   cmsDepartmentMapper.deleteBatchIds(id);
        if(result >=1){
            return CommonResult.success("删除成功!");
        }else {
            return CommonResult.failed("删除失败！");
        }
    }

    @Override
    public CommonResult updateCmsDepartment(CmsDepartmentUpdateDto updateDepartment) {
        SysUser sysUser = userMapper.selectOne(new QueryWrapper<SysUser>().lambda()
                .eq(SysUser::getNumber, updateDepartment.getUserNumber())
                .eq(SysUser::getName,updateDepartment.getUserName()));
        if (ObjectUtil.isEmpty(sysUser)){
            return CommonResult.failed("未找到该负责人用户");
        }
        CmsDepartment cmsDepartment = new CmsDepartment();
        BeanUtil.copyProperties(updateDepartment,cmsDepartment);
        int result =  cmsDepartmentMapper.updateById(cmsDepartment);
        if(result >=1){
            return CommonResult.success("修改成功!");
        }else {
            return CommonResult.failed("修改失败！");
        }
    }

    @Override
    public List<Map<String, Object>> getDepartmentList() {
        List<Map<String, Object>> resultList = new ArrayList<>();
        List<CmsDepartment> cmsDepartments = cmsDepartmentMapper.selectList(null);
        cmsDepartments.stream().forEach(
                cmsDepartment -> {
                    Map<String, Object> department = new HashMap<>();
                    department.put("id", cmsDepartment.getId().toString());
                    department.put("name", cmsDepartment.getName());
                    resultList.add(department);
                }
        );
        return resultList;
    }

    @Override
    public List<Map<String, Object>> getBasDepartmentList() {
        List<Map<String, Object>> resultList = new ArrayList<>();
        List<CmsDepartment> cmsDepartments = cmsDepartmentMapper.selectList(null);
        cmsDepartments.stream().forEach(
                cmsDepartment -> {
                    Map<String, Object> department = new HashMap<>();
                    department.put("departmentNumber", cmsDepartment.getNumber());
                    department.put("departmentName", cmsDepartment.getName());
                    resultList.add(department);
                }
        );
        return resultList;
    }

    @Override
    public Map<String, List<UserBaseInfoVo>> getDepartmentUserList() {
        HashMap<String, List<UserBaseInfoVo>> map = new HashMap<>();
        List<CmsDepartment> cmsDepartments = cmsDepartmentMapper.selectList(null);

        for (CmsDepartment cmsDepartment : cmsDepartments){
            List<SysUser> sysUsers = userMapper.selectList(new QueryWrapper<SysUser>().lambda().eq(SysUser::getDepartmentNumber, cmsDepartment.getNumber()));
            List<UserBaseInfoVo> sysUserVOs = new ArrayList<>();
            for (SysUser sysUser : sysUsers){
                UserBaseInfoVo sysUserVO = new UserBaseInfoVo();
                BeanUtil.copyProperties(sysUser,sysUserVO);
                sysUserVOs.add(sysUserVO);
            }

            map.put(cmsDepartment.getName(), sysUserVOs);
        }

        return map;
    }
}
