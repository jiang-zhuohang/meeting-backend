package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.BasePage;
import com.jhc.DO.CmsAttendance;
import com.jhc.DO.CmsFace;
import com.jhc.DO.SysConfig;
import com.jhc.DTO.CmsAttendanceUpdateDto;
import com.jhc.VO.FaceInfoVo;
import com.jhc.VO.FaceSearchVo;
import com.jhc.VO.MeetingInfoVo;
import com.jhc.engine.api.SearchEngine;
import com.jhc.engine.conf.Constant;
import com.jhc.engine.model.MapParam;
import com.jhc.engine.model.SearchDocument;
import com.jhc.engine.model.SearchResponse;
import com.jhc.engine.model.SearchResult;
import com.jhc.mapper.CmsAttendanceMapper;
import com.jhc.mapper.CmsFaceMapper;
import com.jhc.mapper.SysConfigMapper;
import com.jhc.service.ICmsAttendanceService;
import com.jhc.service.ICmsFaceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.service.ICmsImagesService;
import com.jhc.service.ICmsMeetingService;
import com.jhc.utils.CommonResult;
import com.jhc.utils.DistanceUtil;
import com.jhc.utils.StudioUtils;
import com.visual.face.search.core.domain.ExtParam;
import com.visual.face.search.core.domain.FaceImage;
import com.visual.face.search.core.domain.FaceInfo;
import com.visual.face.search.core.domain.ImageMat;
import com.visual.face.search.core.extract.FaceFeatureExtractor;
import com.visual.face.search.core.utils.JsonUtil;
import com.visual.face.search.core.utils.Similarity;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-06-15
 */
@Service
public class CmsFaceServiceImpl extends ServiceImpl<CmsFaceMapper, CmsFace> implements ICmsFaceService {
    @Autowired
    private SearchEngine searchEngine;
    @Autowired
    private FaceFeatureExtractor faceFeatureExtractor;
    @Autowired
    private ICmsImagesService imagesService;
    @Autowired
    private ICmsMeetingService cmsMeetingService;
    @Autowired
    private ICmsAttendanceService cmsAttendanceService;
    @Autowired
    private CmsFaceMapper cmsFaceMapper;
    @Autowired
    private CmsAttendanceMapper cmsAttendanceMapper;
    @Autowired
    private SysConfigMapper sysConfigMapper;

    @Value("${spring.application.name}")
    private String vectorTableName;

    @Override
    public CommonResult createEngine() {
        if (searchEngine.exist(vectorTableName)){
            return CommonResult.failed("向量引擎初始化:已经初始化");
        }else {
            MapParam param = MapParam.build()
                    .put(Constant.ParamKeyMaxDocsPerSegment,0);
            boolean createVectorFlag = searchEngine.createCollection(vectorTableName, param);
            if (createVectorFlag){
                return CommonResult.success("向量引擎初始化:初始化成功");
            }else return CommonResult.failed("向量引擎初始化:初始化失败");
        }
    }

    @Override
    public CommonResult createFace(MultipartFile file, String userNumber, String userName) {
        //获取特征向量
        //当设置为0f时，会默认使用当前模型的默认值，该方法为推荐使用方式
        ExtParam extParam = ExtParam.build().setMask(true).setScoreTh(0f / 100).setIouTh(0);
        ImageMat imageMat = null;
        FaceImage faceImage = null;
        try {
            imageMat = ImageMat.fromInputStream(file.getInputStream());
            faceImage = faceFeatureExtractor.extract(imageMat, extParam, new HashMap<>());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(null != imageMat){
                imageMat.release();
            }
        }
        if(null == faceImage){
            return CommonResult.failed("FeatureExtractor extract error(提取错误)");
        }
        if(faceImage.faceInfos().size() <= 0){
            return CommonResult.failed("图片不是人脸");
        }
        //图片保存到本地
        String photoPath = imagesService.fileUpload(file);
        if (photoPath.equals("上传失败")) return CommonResult.failed("图片保存到服务器失败");
        //获取分数做好的人脸
        FaceInfo faceInfo = faceImage.faceInfos.get(0);
        float[] embeds = faceInfo.embedding.embeds;

        //插入数据
        CmsFace cmsFace = new CmsFace();
        cmsFace.setUserName(userName);
        cmsFace.setUserNumber(userNumber);
        cmsFace.setFacePhoto(photoPath);
        cmsFace.setFaceScore(faceInfo.score * 100);
        cmsFace.setFaceVector(JsonUtil.toString(embeds));
        cmsFaceMapper.insert(cmsFace);
        //写入数据到人脸向量库
//        searchEngine.deleteVectorByKey(vectorTableName,Long.parseLong("1"));
        boolean flag1 = searchEngine.insertVector(vectorTableName, cmsFace.getId(),userNumber, embeds);
        if(!flag1){
            return CommonResult.failed("写入向量库出错");
        }
        return CommonResult.success("创建成功");
    }

    @Override
    public CommonResult getFaceInfo(String userNumber) {
        CmsFace cmsFace = cmsFaceMapper.selectOne(new QueryWrapper<CmsFace>().lambda().eq(CmsFace::getUserNumber, userNumber));
        if (ObjectUtil.isEmpty(cmsFace)){
            return CommonResult.failed("该用户没有人脸信息");
        }
        FaceInfoVo faceInfoVo = new FaceInfoVo();
        BeanUtil.copyProperties(cmsFace,faceInfoVo);
        return CommonResult.success(faceInfoVo);
    }

    @Override
    public CommonResult updateFace(MultipartFile file, String userNumber, String userName) {
        CmsFace cmsFace = cmsFaceMapper.selectOne(new QueryWrapper<CmsFace>().lambda().eq(CmsFace::getUserNumber, userNumber));
        if (ObjectUtil.isEmpty(cmsFace)){
            return  CommonResult.failed("该用户没有人脸信息");
        }
        //获取特征向量
        //当设置为0f时，会默认使用当前模型的默认值，该方法为推荐使用方式
        ExtParam extParam = ExtParam.build().setMask(true).setScoreTh(0f / 100).setIouTh(0);
        ImageMat imageMat = null;
        FaceImage faceImage = null;
        try {
            imageMat = ImageMat.fromInputStream(file.getInputStream());
            faceImage = faceFeatureExtractor.extract(imageMat, extParam, new HashMap<>());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(null != imageMat){
                imageMat.release();
            }
        }
        if(null == faceImage){
            return CommonResult.failed("FeatureExtractor extract error(提取错误)");
        }
        if(faceImage.faceInfos().size() <= 0){
            return CommonResult.failed("图片不是人脸");
        }
        //删除人脸图片
        imagesService.deleteFile(cmsFace.getFacePhoto());
        //图片保存到本地
        String photoPath = imagesService.fileUpload(file);
        if (photoPath.equals("上传失败")) return CommonResult.failed("图片保存到服务器失败");
        //获取分数做好的人脸
        FaceInfo faceInfo = faceImage.faceInfos.get(0);
        float[] embeds = faceInfo.embedding.embeds;

        //插入数据
        cmsFace.setFacePhoto(photoPath);
        cmsFace.setFaceScore(faceInfo.score * 100);
        cmsFace.setFaceVector(JsonUtil.toString(embeds));
        cmsFace.setUpdateTime(new Date());
        cmsFaceMapper.updateById(cmsFace);
        //更新数据到人脸向量库
        boolean flag1 = searchEngine.updateVector(vectorTableName,cmsFace.getId(), userNumber, embeds);
        if(!flag1){
            return CommonResult.failed("写入向量库出错");
        }
        return CommonResult.success("更新成功");
    }

    @Override
    public CommonResult searchFace(MultipartFile file) {
        //获取特征向量
        //当设置为0f时，会默认使用当前模型的默认值，该方法为推荐使用方式
        ExtParam extParam = ExtParam.build().setMask(true).setScoreTh(0f / 100).setIouTh(0);
        ImageMat imageMat = null;
        FaceImage faceImage = null;
        try {
            imageMat = ImageMat.fromInputStream(file.getInputStream());
            faceImage = faceFeatureExtractor.extract(imageMat, extParam, new HashMap<>());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(null != imageMat){
                imageMat.release();
            }
        }
        if(null == faceImage){
            return CommonResult.failed("FeatureExtractor extract error(提取错误)");
        }
        List<FaceInfo> faceInfos = faceImage.faceInfos();
        if(faceInfos.size() <= 0){
            return CommonResult.failed("图片不是人脸");
        }
        float [][] vectors = new float[faceInfos.size()][];
        for(int i=0; i< faceInfos.size(); i++){
            vectors[i] = faceInfos.get(i).embedding.embeds;
        }
        //特征搜索
        int topK = 1; //一个最符合的结果
        SearchResponse searchResponse =searchEngine.search(vectorTableName, vectors, topK);
        if(!searchResponse.getStatus().ok()){
            return CommonResult.failed(searchResponse.getStatus().getReason());
        }
        if (searchResponse.getResult()==null){
            return CommonResult.failed("匹配不到该人脸，请重试");
        }
        //结果和人数是否一致
        List<SearchResult> result = searchResponse.getResult();
        if(result.size() != faceInfos.size()){
            return CommonResult.failed("search result error");
        }
        //返回前端
        SearchDocument searchDocument = result.get(0).getDocuments().get(0);
        CmsFace cmsFace = cmsFaceMapper.selectById(searchDocument.getPrimaryKey());
        float[] faceVector = StudioUtils.convertVector(cmsFace.getFaceVector());
        float simVal = Similarity.cosineSimilarity(faceInfos.get(0).embedding.embeds, faceVector);
        float confidence = (float) Math.floor(simVal * 10000)/100;
        if (confidence < 70){
            return CommonResult.failed("匹配不到该人脸，请重试");
        }
        FaceSearchVo faceSearchVo = new FaceSearchVo();
        faceSearchVo.setUserName(cmsFace.getUserName());
        faceSearchVo.setUserNumber(cmsFace.getUserNumber());
        faceSearchVo.setFacePhoto(cmsFace.getFacePhoto());
        faceSearchVo.setConfidence(confidence);
        return CommonResult.success(faceSearchVo);
    }

    @Override
    public CommonResult faceSignOnline(MultipartFile file, Long meetingId, String number) {
        MeetingInfoVo meeting = cmsMeetingService.getCmsMeetingById(meetingId);
        if (meeting.getMeetingStatus()!=3 && meeting.getMeetingStatus()!=4 && meeting.getMeetingStatus()!=5){
            return CommonResult.failed("不在签到/签退时间");
        }

        CmsAttendance cmsAttendance = cmsAttendanceMapper.selectOne(new QueryWrapper<CmsAttendance>().lambda()
                .eq(CmsAttendance::getMeetingId, meetingId)
                .eq(CmsAttendance::getUserNumber, number));

        if (cmsAttendance.getSignInStatus()==0 && meeting.getMeetingStatus()==5){
            return CommonResult.failed("您已缺席!");
        }

        if (cmsAttendance.getSignInStatus()!=0 && meeting.getMeetingStatus()!=5){
            return CommonResult.failed("您已签到过!");
        }

        CommonResult faceCommonResult = searchFace(file);
        if (faceCommonResult.getCode()!=200){
            return CommonResult.failed(faceCommonResult.getMessage());
        }

        FaceSearchVo faceSearchVo = (FaceSearchVo) faceCommonResult.getData();
            if (ObjectUtil.isEmpty(faceSearchVo)){
                return CommonResult.failed("未匹配到人脸，请重试!");
            }
            //成功匹配当前用户
            if (faceSearchVo.getUserNumber().equals(number)){
                CmsAttendanceUpdateDto cmsAttendanceUpdateDto = new CmsAttendanceUpdateDto();
                BeanUtil.copyProperties(cmsAttendance,cmsAttendanceUpdateDto);
                //考勤表修改
                if (cmsAttendance.getSignInStatus()==0){//未签到，要签到
                    cmsAttendanceUpdateDto.setSignInMethod(1);//方法为人脸签到
                    cmsAttendanceUpdateDto.setSignInTime(LocalDateTime.now());
                    if (meeting.getMeetingStatus()==3){//正常
                        cmsAttendanceUpdateDto.setSignInStatus(1);
                    }else {                             //迟到
                        cmsAttendanceUpdateDto.setSignInStatus(2);
                    }
                }else {                                 //已签到，要签退
                    cmsAttendanceUpdateDto.setSignOutMethod(1);
                    cmsAttendanceUpdateDto.setSignOutTime(LocalDateTime.now());
                    if (meeting.getMeetingStatus()==5){
                        cmsAttendanceUpdateDto.setSignOutStatus(1);
                    }else {
                        cmsAttendanceUpdateDto.setSignOutStatus(2);
                    }
                }
                return cmsAttendanceService.updateCmsAttendance(cmsAttendanceUpdateDto);
            }else {
                return CommonResult.failed("签到失败，不是该用户人脸");
            }

    }

    @Override
    public CommonResult faceSignOffline(MultipartFile file, Long meetingId, String number, Double lng, Double lat) {
        SysConfig sysConfig = sysConfigMapper.selectOne(null);
        if (ObjectUtil.isEmpty(sysConfig)){
            return CommonResult.failed("系统未设置签到位置范围，请联系管理员");
        }
        //位置范围判定
        Double radius = sysConfig.getRadius()*0.001;//系统半径 km
        //用户半径 km
        double userRadius = DistanceUtil.getDistance(sysConfig.getLng(), sysConfig.getLat(), lng, lat);
        if (userRadius > radius) {
            return CommonResult.failed("你的位置不在签到范围内!");
        }

        return faceSignOnline(file,meetingId,number);
    }

    @Override
    public CommonResult checkFace(MultipartFile file,String number) {
        CmsFace cmsFace = cmsFaceMapper.selectOne(new QueryWrapper<CmsFace>().lambda().eq(CmsFace::getUserNumber, number));
        //获取特征向量
        //当设置为0f时，会默认使用当前模型的默认值，该方法为推荐使用方式
        ExtParam extParam = ExtParam.build().setMask(true).setScoreTh(0f / 100).setIouTh(0);
        ImageMat imageMat = null;
        FaceImage faceImage = null;
        try {
            imageMat = ImageMat.fromInputStream(file.getInputStream());
            faceImage = faceFeatureExtractor.extract(imageMat, extParam, new HashMap<>());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(null != imageMat){
                imageMat.release();
            }
        }
        if(null == faceImage){
            return CommonResult.failed("FeatureExtractor extract error(提取错误)");
        }
        List<FaceInfo> faceInfos = faceImage.faceInfos();
        if(faceInfos.size() <= 0){
            return CommonResult.failed("图片不是人脸");
        }

        float[] faceVector = StudioUtils.convertVector(cmsFace.getFaceVector());
        float simVal = Similarity.cosineSimilarity(faceInfos.get(0).embedding.embeds, faceVector);
        float confidence = (float) Math.floor(simVal * 10000)/100;
        return CommonResult.success("置信度:"+confidence);
    }

    @Override
    public IPage<FaceInfoVo> getCmsFacePageData(BasePage pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        return cmsFaceMapper.getFacePageData(page);
    }

    @Override
    public CommonResult deleteCmsFaceById(List<Long> list) {
        List<CmsFace> cmsFaces = cmsFaceMapper.selectBatchIds(list);
        if (ObjectUtil.isEmpty(cmsFaces)){
            CommonResult.failed("失败,没有该数据");
        }
        for (CmsFace cmsFace : cmsFaces){
            searchEngine.deleteVectorByKey(vectorTableName,cmsFace.getId());
            imagesService.deleteFile(cmsFace.getFacePhoto());
        }
        int i = cmsFaceMapper.deleteBatchIds(list);
        if (i > 0) {
            return CommonResult.success("删除成功");
        }
        return CommonResult.failed("删除失败");
    }

}
