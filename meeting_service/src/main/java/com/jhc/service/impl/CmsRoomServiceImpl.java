package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsRoom;
import com.jhc.DO.CmsRoom;
import com.jhc.DTO.*;
import com.jhc.VO.CmsRoomVo;
import com.jhc.VO.CmsRoomVo;
import com.jhc.mapper.CmsRoomMapper;
import com.jhc.mapper.CmsRoomMapper;
import com.jhc.service.ICmsRoomService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
@Service
public class CmsRoomServiceImpl extends ServiceImpl<CmsRoomMapper, CmsRoom> implements ICmsRoomService {

    @Autowired
    private CmsRoomMapper cmsRoomMapper;

    @Override
    public IPage<CmsRoomVo> getCmsRoomPageData(CmsRoomDto pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        IPage<CmsRoomVo>  result =  cmsRoomMapper.getRoomPageData(page,pageVo);
        return result;
    }

    @Override
    public CommonResult addCmsRoom(CmsRoomAddDto roomAddDto) {
        CmsRoom cmsRoom = new CmsRoom();
        BeanUtil.copyProperties(roomAddDto,cmsRoom);
        int result =  cmsRoomMapper.insert(cmsRoom);
        if(result >=1){
            return CommonResult.success("新增成功!");
        }else {
            return CommonResult.failed("新增失败！");
        }
    }

    @Override
    public CommonResult deleteCmsRoom(List<Long> id) {
        int result =   cmsRoomMapper.deleteBatchIds(id);
        if(result >=1){
            return CommonResult.success("删除成功!");
        }else {
            return CommonResult.failed("删除失败！");
        }
    }

    @Override
    public CommonResult updateCmsRoom(CmsRoomUpdateDto updateRoom) {
        CmsRoom cmsRoom = new CmsRoom();
        BeanUtil.copyProperties(updateRoom,cmsRoom);
        int result =  cmsRoomMapper.updateById(cmsRoom);
        if(result >=1){
            return CommonResult.success("修改成功!");
        }else {
            return CommonResult.failed("修改失败！");
        }
    }

    @Override
    public List<Map<String, Object>> getRoomsByStatus(Long status) {
        List<CmsRoom> cmsRooms = cmsRoomMapper.selectList(new QueryWrapper<CmsRoom>().lambda().eq(CmsRoom::getStatus, status));
        List<Map<String,Object>> resultList = new ArrayList<>();
        for (CmsRoom cmsRoom : cmsRooms) {
            Map<String, Object> room = new HashMap<>();
            room.put("number",cmsRoom.getNumber());
            room.put("name",cmsRoom.getName());
            room.put("size",cmsRoom.getSize());
            resultList.add(room);
        }
        return resultList;
    }
}
