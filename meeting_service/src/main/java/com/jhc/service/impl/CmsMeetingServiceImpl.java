package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.BO.AdminSaveDetails;
import com.jhc.DO.CmsAttendance;
import com.jhc.DO.CmsMeeting;
import com.jhc.DO.CmsRoom;
import com.jhc.DO.SysUser;
import com.jhc.DTO.*;
import com.jhc.VO.*;
import com.jhc.mapper.CmsAttendanceMapper;
import com.jhc.mapper.CmsMeetingMapper;
import com.jhc.mapper.CmsRoomMapper;
import com.jhc.mapper.SysUserMapper;
import com.jhc.service.ICmsAttendanceService;
import com.jhc.service.ICmsMeetingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.service.RedisService;
import com.jhc.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@Service
public class CmsMeetingServiceImpl extends ServiceImpl<CmsMeetingMapper, CmsMeeting> implements ICmsMeetingService {

    @Autowired
    private CmsMeetingMapper cmsMeetingMapper;

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private CmsRoomMapper roomMapper;

    @Autowired
    private ICmsAttendanceService cmsAttendanceService;

    @Autowired
    private AdminSaveDetails saveDetails;

    @Autowired
    private RedisService redisService;

    private static final String CACHE_PUNCH_REGION = "MEETING";

    //同步更新redis
    private void updateRedis(CmsMeeting cmsMeeting,int update){
        if (update > 0){
            redisService.set(CACHE_PUNCH_REGION,cmsMeeting.getId().toString(),JSONObject.toJSONString(cmsMeeting));
        }
    }

    @Override
    public CommonResult push() {
        List<CmsMeeting> cmsMeetings = cmsMeetingMapper.selectList(new QueryWrapper<CmsMeeting>().lambda().eq(CmsMeeting::getMeetingStatus,0));
        for (CmsMeeting cmsMeeting : cmsMeetings){
            redisService.set(CACHE_PUNCH_REGION,cmsMeeting.getId().toString(),JSONObject.toJSONString(cmsMeeting));
        }
        return CommonResult.success("同步成功");
    }

    @Override
    public Boolean ifApproval() {
        List<CmsMeeting> cmsMeetings = redisService.get(CACHE_PUNCH_REGION, CmsMeeting.class);
        if (ObjectUtil.isEmpty(cmsMeetings)){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public IPage<CmsMeetingVo> getCmsMeetingPageData(CmsMeetingDto pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        IPage<CmsMeetingVo> result = cmsMeetingMapper.getCmsMeetingPage(page, pageVo);
        return result;
    }

    @Override
    public CommonResult getUserInitiatedMeetingList(String userNumber, String status) {
        List<CmsMeeting> cmsMeetings = cmsMeetingMapper.selectList(new QueryWrapper<CmsMeeting>().lambda().eq(CmsMeeting::getUserNumber, userNumber).orderByDesc(CmsMeeting::getStartTime));
        List<CmsMeeting> meetings = new ArrayList<>();
        List<CmsMeeting> historyMeetings = new ArrayList<>();
        for (CmsMeeting cmsMeeting : cmsMeetings){
            cmsMeeting=getBaseMeetingInfo(cmsMeeting);
            if (cmsMeeting.getMeetingStatus()!=6){
                meetings.add(cmsMeeting);
            }else {
                historyMeetings.add(cmsMeeting);
            }
        }

        if (status.equals("1")){
            return CommonResult.success(meetings);
        }else if (status.equals("2")){
            return CommonResult.success(historyMeetings);
        }
        return CommonResult.success(cmsMeetings);
    }

    @Override
    public CommonResult getUserJoinMeetingList(String userNumber, String status) {

        if (status.equals("2")){
            return CommonResult.success(cmsMeetingMapper.getUserJoinMeetingListHistory(userNumber));
        }
        List<CmsMeeting> userJoinMeetingList = cmsMeetingMapper.getUserJoinMeetingList(userNumber);
        List<CmsMeeting> meetings = new ArrayList<>();
        List<CmsMeeting> historyMeetings = new ArrayList<>();


        for (CmsMeeting cmsMeeting : userJoinMeetingList){
            cmsMeeting = getBaseMeetingInfo(cmsMeeting);
            if (cmsMeeting.getMeetingStatus()==6){
                historyMeetings.add(cmsMeeting);
            }else {
                meetings.add(cmsMeeting);
            }
        }

        if (status.equals("1")){
            return CommonResult.success(meetings);
        }
        return CommonResult.success(historyMeetings);

    }

    @Override
    public CommonResult addCmsMeeting(CmsMeetingAddDto meetingAddDto) {
        if (ObjectUtil.isNotEmpty(meetingAddDto.getRoomNumber()) && meetingAddDto.getType()==0){
            return CommonResult.failed("线上会议无需会议室");
        }
        if (meetingAddDto.getType()==1){
            if (ObjectUtil.isEmpty(meetingAddDto.getRoomNumber())){
                return CommonResult.failed("请设置会议室");
            }
            List<CmsMeeting> cmsMeetings = cmsMeetingMapper.selectList(new QueryWrapper<CmsMeeting>().lambda()
                    .eq(CmsMeeting::getRoomNumber, meetingAddDto.getRoomNumber())
                    .eq(CmsMeeting::getMeetingStatus, 1)
                    .lt(CmsMeeting::getStartTime,meetingAddDto.getEndTime())
                    .gt(CmsMeeting::getEndTime,meetingAddDto.getStartTime()));
            if (ObjectUtil.isNotEmpty(cmsMeetings)){
                return CommonResult.failed("会议室该时间段已被占用，请重新编辑");
            }
        }
        CmsMeeting cmsMeeting1 = cmsMeetingMapper.selectOne(new QueryWrapper<CmsMeeting>().lambda()
                .eq(CmsMeeting::getName, meetingAddDto.getName()));
        //判断名字重复
        if (ObjectUtil.isEmpty(cmsMeeting1)) {
            CmsMeeting cmsMeeting = new CmsMeeting();
            BeanUtil.copyProperties(meetingAddDto, cmsMeeting);

            cmsMeeting.setMemberNumber(String.join(",",meetingAddDto.getMemberNumber()));

            int result = cmsMeetingMapper.insert(cmsMeeting);
            if (result >= 1) {
                if (saveDetails.getSysUser().getType()==10){
                    redisService.set(CACHE_PUNCH_REGION,cmsMeeting.getId().toString(), JSONObject.toJSONString(cmsMeeting));
                }
                return CommonResult.success("新增成功!");
            } else {
                return CommonResult.failed("新增失败！");
            }
        }
        return CommonResult.failed("已经有当前会议名称，请重新取名");
    }

    @Override
    public CommonResult deleteCmsMeeting(List<Long> list) {
        int result = cmsMeetingMapper.deleteBatchIds(list);
        if (result >= 1) {
            for (Long s : list){
                redisService.remove(CACHE_PUNCH_REGION,s.toString());
            }
            return CommonResult.success("删除成功!");
        } else {
            return CommonResult.failed("删除失败！");
        }
    }

    @Override
    public CommonResult updateCmsMeeting(CmsMeetingUpdateDto updateMeeting) {
        if (ObjectUtil.isNotEmpty(updateMeeting.getType())){
            if (ObjectUtil.isNotEmpty(updateMeeting.getRoomNumber()) && updateMeeting.getType()==0){
                return CommonResult.failed("线上会议无需会议室");
            }
            if (updateMeeting.getType()==1){
                if (ObjectUtil.isEmpty(updateMeeting.getRoomNumber())){
                    return CommonResult.failed("请设置会议室");
                }
                List<CmsMeeting> cmsMeetings = cmsMeetingMapper.selectList(new QueryWrapper<CmsMeeting>().lambda()
                        .eq(CmsMeeting::getRoomNumber, updateMeeting.getRoomNumber())
                        .eq(CmsMeeting::getMeetingStatus, 1)
                        .lt(CmsMeeting::getStartTime,updateMeeting.getEndTime())
                        .gt(CmsMeeting::getEndTime,updateMeeting.getStartTime()));
                if (ObjectUtil.isNotEmpty(cmsMeetings)){
                    if (updateMeeting.getMeetingStatus()==1){
                        return CommonResult.failed("会议室该时间段已被占用，请拒绝申请");
                    }
                    return CommonResult.failed("会议室该时间段已被占用，请重新编辑");
                }
            }
        }

        CmsMeeting oldMeeting = cmsMeetingMapper.selectById(updateMeeting.getId());
        Integer oldStatus = oldMeeting.getMeetingStatus();
        if (updateMeeting.getMeetingStatus()==0 && oldStatus!=0 && oldStatus!=2){
            return CommonResult.failed("审核中或未通过的会议才可修改内容");
        }
        if (updateMeeting.getMeetingStatus()==1){
            if (oldStatus==0){
                List<String> memberNumbers =  Arrays.asList(oldMeeting.getMemberNumber().split(","));
                List<SysUser> sysUsers = userMapper.selectList(new QueryWrapper<SysUser>().lambda().in(SysUser::getNumber, memberNumbers));
                CmsAttendanceAddDto cmsAttendanceAddDto = new CmsAttendanceAddDto();
                for (SysUser user : sysUsers){
                    cmsAttendanceAddDto.setUserNumber(user.getNumber());
                    cmsAttendanceAddDto.setUserName(user.getName());
                    cmsAttendanceAddDto.setMeetingName(oldMeeting.getName());
                    cmsAttendanceAddDto.setMeetingId(updateMeeting.getId());
                    CommonResult commonResult = cmsAttendanceService.addCmsAttendance(cmsAttendanceAddDto);
                    if (commonResult.getCode()==500){
                        return commonResult;
                    }
                }
            }
        }
        if (updateMeeting.getMeetingStatus()==2 && oldStatus!=0){
            return CommonResult.failed("审批状态违规");
        }

        BeanUtil.copyProperties(updateMeeting, oldMeeting);
        if (ObjectUtil.isNotEmpty(updateMeeting.getMemberNumber())){
            oldMeeting.setMemberNumber(String.join(",",updateMeeting.getMemberNumber()));
        }

        int result = cmsMeetingMapper.updateById(oldMeeting);
        //同步redis
        if (updateMeeting.getMeetingStatus()!=0){
            redisService.remove(CACHE_PUNCH_REGION,updateMeeting.getId().toString());
        }else {
            updateRedis(oldMeeting,result);
        }
        if (result >= 1) {
            return CommonResult.success("修改成功!");
        } else {
            return CommonResult.failed("修改失败！");
        }
    }

    @Override
    public MeetingInfoVo getCmsMeetingById(Long id) {
        CmsMeeting cmsMeeting = cmsMeetingMapper.selectById(id);

        if (ObjectUtil.isEmpty(cmsMeeting)){
            return null;
        }
        cmsMeeting = getBaseMeetingInfo(cmsMeeting);
        MeetingInfoVo meetingInfoVo = new MeetingInfoVo();
        BeanUtil.copyProperties(cmsMeeting,meetingInfoVo);

        //是否有成员
        if (ObjectUtil.isNotEmpty(cmsMeeting.getMemberNumber())) {
            List<UserBaseInfoVo> members = new ArrayList<>();
            List<String> memberNumbers =  Arrays.asList(cmsMeeting.getMemberNumber().split(","));
            for (String number : memberNumbers){
                SysUser sysUser = userMapper.selectOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getNumber, number));
                if (ObjectUtil.isNotEmpty(sysUser)){
                    UserBaseInfoVo userBaseInfoVo = new UserBaseInfoVo();
                    BeanUtil.copyProperties(sysUser,userBaseInfoVo);
                    members.add(userBaseInfoVo);
                }
            }
            meetingInfoVo.setMember(members);
        }

        //是否线下
        if (ObjectUtil.isNotEmpty(cmsMeeting.getRoomNumber())){
            CmsRoom cmsRoom = roomMapper.selectOne(new QueryWrapper<CmsRoom>().lambda().eq(CmsRoom::getNumber, cmsMeeting.getRoomNumber()));
            meetingInfoVo.setRoomName(cmsRoom.getName());
        }

        return meetingInfoVo;
    }

    @Override
    public CommonResult getCmsMeetingList(String status) {
        List<CmsMeeting> cmsMeetings = null;
        if (status == null){
//            cmsMeetings = redisService.get(CACHE_PUNCH_REGION, CmsMeeting.class);
//            if (ObjectUtil.isNotEmpty(cmsMeetings)){
//                Collections.sort(cmsMeetings, new Comparator<CmsMeeting>() {
//                    @Override
//                    public int compare(CmsMeeting o1, CmsMeeting o2) {
//                        return (o2.getStartTime().compareTo(o1.getStartTime()));
//                    }
//                });
//                return CommonResult.success(cmsMeetings);
//            }
            cmsMeetings = cmsMeetingMapper.selectList(new QueryWrapper<CmsMeeting>().lambda()
                    .last("order by case when meeting_status = '0' then 1 else 2 end,start_time desc"));
        }else {
            cmsMeetings = cmsMeetingMapper.selectList(new QueryWrapper<CmsMeeting>().lambda()
                    .eq(CmsMeeting::getMeetingStatus,status));
        }
        return CommonResult.success(cmsMeetings);
    }

    /**
     * 给自己会议成员手动签到
     * @return
     */
    @Override
    public CommonResult manualSign(ManualSignDto manualSignDto) {
        String meetingUserNumber = saveDetails.getSysUser().getNumber();
        CmsMeeting cmsMeeting = cmsMeetingMapper.selectOne(new QueryWrapper<CmsMeeting>().lambda().eq(CmsMeeting::getId, manualSignDto.getMeetingId())
                .eq(CmsMeeting::getUserNumber, meetingUserNumber));
        if (ObjectUtil.isEmpty(cmsMeeting)){
            return CommonResult.failed("没有权限");
        }
//        CmsAttendance cmsAttendance = new CmsAttendance();
//        cmsAttendance.setUserNumber(userNumber);
//        cmsAttendance.setUserName(userName);
//        return cmsAttendanceService.updateManualSign(cmsAttendance,new QueryWrapper<CmsAttendance>().lambda().eq(CmsAttendance::getMeetingId,meetingId).eq(CmsAttendance::getUserNumber,userNumber));
        CmsAttendance cmsAttendance = cmsAttendanceService.getOne(new QueryWrapper<CmsAttendance>().lambda().eq(CmsAttendance::getMeetingId, manualSignDto.getMeetingId())
                .eq(CmsAttendance::getUserNumber, manualSignDto.getUserNumber()));
        if (ObjectUtil.isEmpty(cmsAttendance)){
            return CommonResult.failed("会议中没有该成员");
        }
        CmsAttendanceUpdateDto cmsAttendanceUpdateDto = new CmsAttendanceUpdateDto();
        BeanUtil.copyProperties(cmsAttendance,cmsAttendanceUpdateDto);
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime endTime = cmsMeeting.getEndTime();
        if (now.isBefore(endTime)){
            if (cmsAttendance.getSignInStatus()==0) {//未签到，要签到
                cmsAttendanceUpdateDto.setSignInMethod(2);//方法为手动签到
                cmsAttendanceUpdateDto.setSignInTime(LocalDateTime.now());
                cmsAttendanceUpdateDto.setSignInStatus(1);
            }
        }else {
            if (cmsAttendance.getSignOutStatus()==0) {//未签到，要签到
                cmsAttendanceUpdateDto.setSignOutMethod(2);//方法为手动签到
                cmsAttendanceUpdateDto.setSignOutTime(LocalDateTime.now());
                cmsAttendanceUpdateDto.setSignOutStatus(1);
            }
        }

        return cmsAttendanceService.updateCmsAttendance(cmsAttendanceUpdateDto);
    }

    @Override
    public CommonResult endMeeting(Long meetingId) {
        CmsMeeting cmsMeeting = cmsMeetingMapper.selectOne(new QueryWrapper<CmsMeeting>().lambda()
                .eq(CmsMeeting::getId, meetingId)
                .eq(CmsMeeting::getUserNumber, saveDetails.getSysUser().getNumber()));
        if (ObjectUtil.isEmpty(cmsMeeting)){
            return CommonResult.failed("没有权限");
        }
        LocalDateTime now = LocalDateTime.now();
        if (cmsMeeting.getEndTime().isBefore(now)) {
            return CommonResult.failed("会议已经在结束阶段");
        }
        cmsMeeting.setEndTime(now);
        CmsMeetingUpdateDto cmsMeetingUpdateDto = new CmsMeetingUpdateDto();
        BeanUtil.copyProperties(cmsMeeting,cmsMeetingUpdateDto);
        cmsMeetingUpdateDto.setType(null);
        return updateCmsMeeting(cmsMeetingUpdateDto);
    }


    private CmsMeeting getBaseMeetingInfo(CmsMeeting cmsMeeting){
        //审核通过的会议状态
        if (cmsMeeting.getMeetingStatus()==1){
            LocalDateTime now = LocalDateTime.now();//当前时间
            LocalDateTime startTime = cmsMeeting.getStartTime();//会议开始时间
            LocalDateTime endTime = cmsMeeting.getEndTime();//会议结束时间
            Integer timeFrame = cmsMeeting.getTimeFrame();//签到时间范围
            LocalDateTime signStartTime = startTime.minusMinutes(timeFrame);//开始签到的时间
            LocalDateTime signOutTime = endTime.plusMinutes(timeFrame);//结束签退时间

            if (now.isBefore(signStartTime)){
                cmsMeeting.setMeetingStatus(1);
            }else if (now.isBefore(startTime)){
                cmsMeeting.setMeetingStatus(3);
            }else if (now.isBefore(endTime)){
                cmsMeeting.setMeetingStatus(4);
            }else if (now.isBefore(signOutTime)){
                cmsMeeting.setMeetingStatus(5);
            }else {
                cmsMeeting.setMeetingStatus(6);
            }

        }
        return cmsMeeting;
    }
}
