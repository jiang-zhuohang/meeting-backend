package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.BO.AdminSaveDetails;
import com.jhc.DO.CmsAttendance;
import com.jhc.DO.CmsLeave;
import com.jhc.DO.CmsLeave;
import com.jhc.DO.CmsMeeting;
import com.jhc.DTO.*;
import com.jhc.VO.CmsLeaveVo;
import com.jhc.VO.CmsLeaveVo;
import com.jhc.mapper.CmsAttendanceMapper;
import com.jhc.mapper.CmsLeaveMapper;
import com.jhc.mapper.CmsLeaveMapper;
import com.jhc.mapper.CmsMeetingMapper;
import com.jhc.service.ICmsAttendanceService;
import com.jhc.service.ICmsLeaveService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.service.IMessageService;
import com.jhc.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
@Service
public class CmsLeaveServiceImpl extends ServiceImpl<CmsLeaveMapper, CmsLeave> implements ICmsLeaveService {
    
    @Autowired
    private CmsLeaveMapper cmsLeaveMapper;

    @Autowired
    private CmsAttendanceMapper cmsAttendanceMapper;

    @Autowired
    private AdminSaveDetails saveDetails;

    @Autowired
    private IMessageService messageService;

    @Autowired
    private CmsMeetingMapper cmsMeetingMapper;


    @Override
    public IPage<CmsLeaveVo> getCmsLeavePageData(CmsLeaveDto pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        IPage<CmsLeaveVo>  result =  cmsLeaveMapper.getLeavePageData(page,pageVo);
        return result;
    }

    @Override
    public CommonResult addCmsLeave(CmsLeaveAddDto leaveAddDto) {
        List<CmsLeave> cmsLeaves = cmsLeaveMapper.selectList(new QueryWrapper<CmsLeave>().lambda()
                .eq(CmsLeave::getMeetingId, leaveAddDto.getMeetingId())
                .eq(CmsLeave::getUserNumber, leaveAddDto.getUserNumber()));
        if (ObjectUtil.isNotEmpty(cmsLeaves)){
            for (CmsLeave cmsLeave : cmsLeaves){
                if (cmsLeave.getStatus()==1){
                    return CommonResult.failed("您的请假申请已通过");
                }else if (cmsLeave.getStatus()==0){
                    return CommonResult.failed("重复提交请假申请");
                }
            }
        }
        CmsLeave cmsLeave = new CmsLeave();
        BeanUtil.copyProperties(leaveAddDto,cmsLeave);
        int result =  cmsLeaveMapper.insert(cmsLeave);
        if(result >=1){
            CmsMeeting cmsMeeting = cmsMeetingMapper.selectById(leaveAddDto.getMeetingId());
            MessageFormatAddDto messageFormatAddDto = new MessageFormatAddDto();
            messageFormatAddDto.setSendNumber(saveDetails.getSysUser().getNumber());
            messageFormatAddDto.setSendName(saveDetails.getSysUser().getName());
            messageFormatAddDto.setAcceptNumber(cmsMeeting.getUserNumber());
            messageFormatAddDto.setId(leaveAddDto.getMeetingId());
            messageFormatAddDto.setType("10");
            messageFormatAddDto.setTitle("您的会议:"+leaveAddDto.getMeetingName()+" 有新的请假申请!");
            messageService.addFormatMessage(messageFormatAddDto);

            return CommonResult.success("新增成功!");
        }else {
            return CommonResult.failed("新增失败！");
        }
    }

    @Override
    public CommonResult deleteCmsLeave(List<Long> id) {
        int result =   cmsLeaveMapper.deleteBatchIds(id);
        if(result >=1){
            return CommonResult.success("删除成功!");
        }else {
            return CommonResult.failed("删除失败！");
        }
    }

    @Override
    public CommonResult updateCmsLeave(CmsLeaveUpdateDto updateLeave) {
        if (updateLeave.getStatus()!=0){
            CmsLeave cmsLeave = cmsLeaveMapper.selectById(updateLeave.getId());

            MessageFormatAddDto messageFormatAddDto = new MessageFormatAddDto();
            messageFormatAddDto.setSendNumber(saveDetails.getSysUser().getNumber());
            messageFormatAddDto.setSendName(saveDetails.getSysUser().getName());
            messageFormatAddDto.setAcceptNumber(cmsLeave.getUserNumber());
            messageFormatAddDto.setId(cmsLeave.getMeetingId());
            messageFormatAddDto.setType("11");
            if (updateLeave.getStatus()==1){
                CmsAttendance cmsAttendance = cmsAttendanceMapper.selectOne(new QueryWrapper<CmsAttendance>().lambda()
                        .eq(CmsAttendance::getUserNumber, cmsLeave.getUserNumber())
                        .eq(CmsAttendance::getMeetingId, cmsLeave.getMeetingId()));
                cmsAttendance.setIsLeave(1);
                cmsAttendanceMapper.updateById(cmsAttendance);
                messageFormatAddDto.setTitle("会议:"+cmsLeave.getMeetingName()+"请假审批通过!");
                messageService.addFormatMessage(messageFormatAddDto);
            }else {
                messageFormatAddDto.setTitle("会议:"+cmsLeave.getMeetingName()+"请假审批未通过!");
                messageService.addFormatMessage(messageFormatAddDto);
            }
        }


        CmsLeave cmsLeave = new CmsLeave();
        BeanUtil.copyProperties(updateLeave,cmsLeave);
        int result =  cmsLeaveMapper.updateById(cmsLeave);
        if(result >=1){
            return CommonResult.success("修改成功!");
        }else {
            return CommonResult.failed("修改失败！");
        }
    }

    @Override
    public CommonResult getMeetingUserLeave(String meetingId, String userNumber) {
        List<CmsLeave> cmsLeaves = cmsLeaveMapper.selectList(new QueryWrapper<CmsLeave>().lambda()
                .eq(CmsLeave::getMeetingId, meetingId)
                .eq(CmsLeave::getUserNumber, userNumber));
        return CommonResult.success(cmsLeaves);
    }
}
