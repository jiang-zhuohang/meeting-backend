package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.BaseDo;
import com.jhc.DO.Message;
import com.jhc.DTO.*;
import com.jhc.VO.MessagePageVo;
import com.jhc.VO.MessageVO;
import com.jhc.mapper.MessageMapper;
import com.jhc.service.IMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.service.RedisService;
import com.jhc.utils.CommonResult;
import com.jhc.utils.StudioUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * 消息表 服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-07
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {
    @Resource
    private MessageMapper messageMapper;

    @Autowired
    private RedisService redisService;

    private static final String CACHE_PUNCH_REGION = "MESSAGE";
    private static final long EXPIRE_TIME = 2592000;

    private void updateRedis(Message m,int i){
        if(i > 0) {
            redisService.set(CACHE_PUNCH_REGION,m.getId().toString(),JSONObject.toJSONString(m),EXPIRE_TIME);
        }
    }

    @Override
    public CommonResult push() {
        List<Message> messages = messageMapper.selectList(null);
        for(Message m : messages){
            redisService.set(CACHE_PUNCH_REGION, m.getId().toString(), JSONObject.toJSONString(m),EXPIRE_TIME);
        }
        return CommonResult.success("同步成功");
    }

    @Override
    public CommonResult addMessage(MessageAddDto messageAddDto) {
        Message message = new Message();
        BeanUtil.copyProperties(messageAddDto,message);
        int insert = messageMapper.insert(message);
        //同步更新redis
        updateRedis(message,insert);
        if(insert != 0) return CommonResult.success("添加成功");
        return CommonResult.failed("添加失败");
    }

    @Override
    public CommonResult addFormatMessage (MessageFormatAddDto dto) {
        Message message = new Message ();
        message.setSendNumber (dto.getSendNumber ());
        message.setAcceptNumber (dto.getAcceptNumber ());
        message.setSendName(dto.getSendName());
        message.setType (dto.getType ());
        message.setContent (dto.getId () + "&" + dto.getTitle () );
        int insert = messageMapper.insert (message);

        if (insert != 0){
            //同步更新redis
            message.setUpdateTime(new Date());
            message.setIsRead(false);
            updateRedis(message,insert);
            return CommonResult.success ("操作成功");
        }
        return CommonResult.failed ("操作失败");
    }

    @Override
    public boolean deleteMessageById(List<Long> list) {
        for (Long l : list){
            int i = messageMapper.deleteBatchIds(list);
            redisService.remove(CACHE_PUNCH_REGION,l.toString());
            if (i == 0) return false;
        }
        return true;
    }

    @Override
    public boolean updateMessageById(MessageUpdateDto messageUpdateDto) {
        Message message = messageMapper.selectOne(new QueryWrapper<Message>().lambda()
                .eq(Message::getId,messageUpdateDto.getId()));

        //添加
        if (ObjectUtil.isNotEmpty(message)) {
            Message message1=new Message();
            BeanUtil.copyProperties(messageUpdateDto, message1);
            int update = messageMapper.updateById(message1);
            updateRedis(message1,update);
            return update != 0;
        }else {
            return false;
        }
    }

    @Override
    public IPage<MessageVO> getMessagePageData(MessageDto pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        return messageMapper.getPageData(page,pageVo);
    }

    @Override
    public IPage<MessagePageVo> getUserMessageData (MessagePageDto dto) {
        List<Message> messages1 = redisService.get(CACHE_PUNCH_REGION, Message.class);
        if (ObjectUtil.isNotEmpty(messages1)){
            for (int i=0;i<messages1.size();i++){
                if (!messages1.get(i).getAcceptNumber().equals(dto.getUserNumber())){
                    messages1.remove(i);
                    i--;
                }
            }
            if (ObjectUtil.isNotEmpty(messages1))
                return StudioUtils.listPage(messages1,dto.getCurrentPage(),dto.getPageSize());
        }
//        效率对比
//        a)、ArrayList对随机访问比较快，而for循环中使用的get()方法，采用的即是随机访问的方法，因此在ArrayList里for循环快。
//        b)、LinkedList则是顺序访问比较快，Iterator中的next()方法采用的是顺序访问方法，因此在LinkedList里使用Iterator较快。
//        c)、主要还是要依据集合的数据结构不同的判断。
//        if (ObjectUtil.isNotEmpty(messages1)) {
//            Iterator<Message> iterator = messages1.iterator();
//            while (iterator.hasNext()) {
//                Message next = iterator.next();
//                if (!next.getAcceptNumber().equals(dto.getUserNumber())) {
//                    iterator.remove();
//                }
//            }
//            if (ObjectUtil.isNotEmpty(messages1)) {
//                IPage iPage = StudioUtils.listPage(messages1, dto.getCurrentPage(), dto.getPageSize());
//                return iPage;
//            }
//        }
        List<Message> messages = messageMapper.selectList (new QueryWrapper<Message> ().lambda ()
                .eq (Message::getAcceptNumber, dto.userNumber)
                .orderByAsc (Message::getIsRead)
                .orderByDesc (BaseDo::getCreateTime));
        if (ObjectUtil.isNotEmpty (messages)){
            List<MessagePageVo> list = new ArrayList<>();
            for (Message m:messages
            ) {
                MessagePageVo vo = new MessagePageVo ();
                vo.setId (m.getId ());
                vo.setContent (m.getContent ());
                vo.setIsRead (m.getIsRead ());
//                换名字了这里是创建时间
                vo.setUpdateTime (m.getCreateTime ());
                list.add (vo);
            }
            if (ObjectUtil.isNotEmpty (list)){
                IPage page = StudioUtils.listPage (list, dto.getCurrentPage (), dto.getPageSize ());
                return page;
            }
            return null;
        }
        return null;
    }

    @Override
    public CommonResult updateIsRead (List<MessageUpdateIsReadDto> dtoList) {
        if (ObjectUtil.isNotEmpty (dtoList)) {
            int i = 0;
            for (MessageUpdateIsReadDto dto:
                    dtoList) {
                Message message = new Message ();
                BeanUtil.copyProperties (dto, message);
                i += messageMapper.updateById (message);
                String m = redisService.get(CACHE_PUNCH_REGION, dto.getId().toString());
                Message message1 = JSONObject.parseObject(m, Message.class);
                message1.setIsRead(dto.getIsRead());
                updateRedis(message1,1);
            }
            System.out.println (i);
            System.out.println (dtoList.size ());
            if (i == dtoList.size ()) {
                return CommonResult.success ("批量修改成功");
            }
            return CommonResult.failed ("批量修改失败");
        }
        return CommonResult.failed ("暂无信息");
    }

}
