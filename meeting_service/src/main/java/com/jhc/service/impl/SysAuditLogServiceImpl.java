package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.DO.SysAuditLog;
import com.jhc.DO.SysUser;
import com.jhc.mapper.SysAuditLogMapper;
import com.jhc.mapper.SysUserMapper;
import com.jhc.service.ISysAuditLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 访问日志信息  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-07-02
 */
@Service
public class SysAuditLogServiceImpl extends ServiceImpl<SysAuditLogMapper, SysAuditLog> implements ISysAuditLogService {

    @Autowired
    private SysUserMapper userMapper;

    @Override
    public Integer logError() {
        Integer errorCount = baseMapper.selectCount(new QueryWrapper<SysAuditLog>().lambda().eq(SysAuditLog::getType, 20));
        return errorCount;
    }

    @Override
    public Integer loginCount() {
        Calendar c = Calendar.getInstance();
        Date today = c.getTime();
        c.add(Calendar.DATE, -1);
        Date yesterday = c.getTime();
        List<SysAuditLog> loginCount = baseMapper.selectList(new QueryWrapper<SysAuditLog>().lambda()
                .eq(SysAuditLog::getType, 10)
                .between(SysAuditLog::getUpdateTime, yesterday, today)
                .and(o -> o.eq(SysAuditLog::getTitle, "用户登录").or().eq(SysAuditLog::getUrl, "/sysUser/login"))
        );
        List<String> result = new ArrayList<>();
        loginCount.stream().forEach(
                logins -> {
                    if (!result.contains(logins.getAddress())) {
                        result.add(logins.getAddress());
                    }
                }
        );
        Integer count = result.size();
        return count;
    }

    @Override
    public Integer registerCount(String type) {
        Integer registerCount = 0;
        Calendar c = Calendar.getInstance();
        Date today = c.getTime();
        c.add(Calendar.DATE, -1);
        Date yesterday = c.getTime();
        if (ObjectUtil.equal("today", type)) {
            registerCount = userMapper.selectCount(new QueryWrapper<SysUser>().lambda()
                    .between(SysUser::getCreateTime, yesterday, today));
        } else if (ObjectUtil.equal("all", type)) {
            registerCount = userMapper.selectCount(null);
        }
        return registerCount;
    }
}
