package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.DO.CmsNotice;
import com.jhc.DTO.CmsNoticeAddDto;
import com.jhc.DTO.CmsNoticeDto;
import com.jhc.DTO.CmsNoticeUpdateDto;
import com.jhc.mapper.CmsNoticeMapper;
import com.jhc.service.ICmsNoticeService;
import com.jhc.service.RedisService;
import com.jhc.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 公告 服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-01-08
 */
@Service
public class CmsNoticeServiceImpl extends ServiceImpl<CmsNoticeMapper, CmsNotice> implements ICmsNoticeService {
    @Autowired
    private CmsNoticeMapper cmsNoticeMapper;
    @Autowired
    private RedisService redisService;

    @Override
    public CommonResult getCmsNotice(CmsNoticeDto dto) {
        Page page = new Page(dto.getCurrentPage(), dto.getPageSize());
        IPage<CmsNotice> result = cmsNoticeMapper.getCmsNoticePage(page, dto);
        if (ObjectUtil.isNotEmpty(result)) {
            return CommonResult.success(result);
        }
        return null;
    }

    @Override
    public CommonResult deleteCmsNoticeById(List<Long> list) {
        int result = cmsNoticeMapper.deleteBatchIds(list);
        for (Long aLong : list) {
            redisService.remove("NOTICE", JSONObject.toJSONString(aLong));
        }
        if (result >= 1) {

            return CommonResult.success("删除成功!");
        } else {
            return CommonResult.failed("删除失败！");
        }
    }

    @Override
    public CommonResult addCmsNotice(CmsNoticeAddDto dto) {
        CmsNotice cmsNotice1 = cmsNoticeMapper.selectOne(new QueryWrapper<CmsNotice>().lambda()
                .eq(CmsNotice::getTitle, dto.getTitle())
                .eq(CmsNotice::getStatus,1));
        if (ObjectUtil.isNotEmpty(cmsNotice1)){
            return CommonResult.failed("已有同名标题");
        }
        CmsNotice cmsNotice = new CmsNotice();
        BeanUtil.copyProperties(dto, cmsNotice);
        int result = cmsNoticeMapper.insert(cmsNotice);
        if (result >= 1) {
            cmsNotice.setContent("");
            redisService.set("NOTICE", JSONObject.toJSONString(cmsNotice.getId()), JSONObject.toJSONString(cmsNotice));
            return CommonResult.success("新增成功!");
        } else {
            return CommonResult.failed("新增失败！");
        }
    }

    @Override
    public CommonResult updateCmsNotice(CmsNoticeUpdateDto updateDto) {
        CmsNotice cmsNotice1 = cmsNoticeMapper.selectOne(new QueryWrapper<CmsNotice>().lambda()
                .eq(CmsNotice::getTitle, updateDto.getTitle())
                .eq(CmsNotice::getStatus,1));
        if (ObjectUtil.isNotEmpty(cmsNotice1)){
            return CommonResult.failed("已有同名标题");
        }
        CmsNotice cmsNotice = new CmsNotice();
        BeanUtil.copyProperties(updateDto, cmsNotice);
        int result = cmsNoticeMapper.updateById(cmsNotice);
        if (result >= 1) {
            cmsNotice.setContent("");
            redisService.set("NOTICE", JSONObject.toJSONString(cmsNotice.getId()), JSONObject.toJSONString(cmsNotice));
            return CommonResult.success("修改成功!");
        } else {
            return CommonResult.failed("修改失败！");
        }
    }

    @Override
    public CommonResult getCmsNoticeA() {
        List<CmsNotice> cmsNotices = redisService.get("NOTICE", CmsNotice.class);
        if (ObjectUtil.isNotEmpty(cmsNotices)) {
            return CommonResult.success(cmsNotices);
        } else {
            List<CmsNotice> cmsNotices1 = cmsNoticeMapper.selectList(null);
            return CommonResult.success("数据库", cmsNotices1);
        }
    }

    @Override
    public CommonResult getCmsNoticeById(Long id) {
        CmsNotice result = cmsNoticeMapper.selectById(id);
        if (ObjectUtil.isNotEmpty(result)) {
            return CommonResult.success(result);
        }
        return null;
    }


}
