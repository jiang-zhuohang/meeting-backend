package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsStaff;
import com.jhc.DO.CmsStaff;
import com.jhc.DTO.CmsStaffAddDto;
import com.jhc.DTO.CmsStaffDto;
import com.jhc.DTO.CmsStaffUpdateDto;
import com.jhc.VO.CmsStaffVo;
import com.jhc.mapper.CmsStaffMapper;
import com.jhc.mapper.CmsStaffMapper;
import com.jhc.service.ICmsStaffService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@Service
public class CmsStaffServiceImpl extends ServiceImpl<CmsStaffMapper, CmsStaff> implements ICmsStaffService {
    @Autowired
    private CmsStaffMapper cmsStaffMapper;

    @Override
    public IPage<CmsStaffVo> getCmsStaffPageData(CmsStaffDto pageVo) {
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        IPage<CmsStaffVo>  result =  cmsStaffMapper.getStaffPageData(page,pageVo);
        return result;
    }

    @Override
    public CommonResult addCmsStaff(CmsStaffAddDto staffAddDto) {
        CmsStaff cmsStaff = new CmsStaff();
        BeanUtil.copyProperties(staffAddDto,cmsStaff);
        int result =  cmsStaffMapper.insert(cmsStaff);
        if(result >=1){
            return CommonResult.success("新增成功!");
        }else {
            return CommonResult.failed("新增失败！");
        }
    }

    @Override
    public CommonResult deleteCmsStaff(List<Long> id) {
        int result =   cmsStaffMapper.deleteBatchIds(id);
        if(result >=1){
            return CommonResult.success("删除成功!");
        }else {
            return CommonResult.failed("删除失败！");
        }
    }

    @Override
    public CommonResult updateCmsStaff(CmsStaffUpdateDto updateStaff) {
        CmsStaff cmsStaff = new CmsStaff();
        BeanUtil.copyProperties(updateStaff,cmsStaff);
        int result =  cmsStaffMapper.updateById(cmsStaff);
        if(result >=1){
            return CommonResult.success("修改成功!");
        }else {
            return CommonResult.failed("修改失败！");
        }
    }

    @Override
    public List<Map<String, Object>> getStaffList() {
        List<Map<String ,Object>> resultList = new ArrayList<>();
        List<CmsStaff> bacStaffs = baseMapper.selectList(null);
        bacStaffs.stream().forEach(
                staff->{
                    Map<String, Object> cmsStaff = new HashMap<>();
                    cmsStaff.put("userNumber",staff.getNumber());
                    cmsStaff.put("userName",staff.getName());
                    resultList.add(cmsStaff);
                }
        );
        return resultList;
    }

}
