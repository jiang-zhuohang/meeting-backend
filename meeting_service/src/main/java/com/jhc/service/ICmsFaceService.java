package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.BasePage;
import com.jhc.DO.CmsFace;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.VO.FaceInfoVo;
import com.jhc.utils.CommonResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-06-15
 */
public interface ICmsFaceService extends IService<CmsFace> {

    CommonResult createEngine();

    CommonResult createFace(MultipartFile file, String userNumber, String userName);

    CommonResult getFaceInfo(String userNumber);

    CommonResult updateFace(MultipartFile file, String userNumber, String userName);

    CommonResult searchFace(MultipartFile file);

    CommonResult faceSignOnline(MultipartFile file, Long meetingId, String number);

    CommonResult faceSignOffline(MultipartFile file, Long meetingId, String number, Double lng, Double lat);

    CommonResult checkFace(MultipartFile file,String number);

    IPage<FaceInfoVo> getCmsFacePageData(BasePage pageVo);

    CommonResult deleteCmsFaceById(List<Long> list);
}
