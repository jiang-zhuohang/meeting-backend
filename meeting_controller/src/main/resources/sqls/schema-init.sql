/*
Navicat MySQL Data Transfer

Source Server         : 123.60.10.173很贵的GPU
Source Server Version : 80029
Source Host           : 123.60.10.173:3307
Source Database       : meeting_backend

Target Server Type    : MYSQL
Target Server Version : 80029
File Encoding         : 65001

Date: 2022-06-18 21:29:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cms_attendance
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cms_attendance` (
  `id` bigint NOT NULL,
  `user_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户编号',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户姓名',
  `meeting_id` bigint NOT NULL COMMENT '会议id',
  `meeting_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '会议名称',
  `is_leave` int NOT NULL DEFAULT '0' COMMENT '是否请假 0否 1请假',
  `sign_in_status` int NOT NULL DEFAULT '0' COMMENT '签到状态 0未签到 1已签到 2迟到',
  `sign_in_method` int DEFAULT NULL COMMENT '签到方式 1人脸  2手动',
  `sign_in_time` datetime DEFAULT NULL COMMENT '签到时间',
  `sign_out_status` int DEFAULT '0' COMMENT '签退状态 0未签退 1已签退 3早退',
  `sign_out_method` int DEFAULT NULL COMMENT ' 签退方式 1人脸  2手动',
  `sign_out_time` datetime DEFAULT NULL COMMENT '签退时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of cms_attendance
-- ----------------------------

-- ----------------------------
-- Table structure for cms_department
-- ----------------------------

CREATE TABLE IF NOT EXISTS `cms_department` (
  `id` bigint NOT NULL COMMENT 'id',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  `number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门编号',
  `user_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '负责人编号',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '负责人姓名',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of cms_department
-- ----------------------------
INSERT INTO `cms_department` VALUES ('1511374392343400450', '销售部', '002', '0001', '燕双鹰', '\0', '2022-04-06 00:05:09', '2022-04-06 00:05:09');

-- ----------------------------
-- Table structure for cms_face
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cms_face` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键、自增、无意',
  `user_number` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL COMMENT '用户编号',
  `user_name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL COMMENT '用户姓名',
  `face_photo` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL COMMENT '人脸图片',
  `face_score` float NOT NULL COMMENT '人脸分数',
  `face_vector` longtext CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL COMMENT '人脸向量',
  `is_del` bit(1) NOT NULL DEFAULT b'0',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1537430448014102530 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cms_face
-- ----------------------------

-- ----------------------------
-- Table structure for cms_leader
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cms_leader` (
  `id` bigint NOT NULL,
  `is_enable` bit(1) NOT NULL DEFAULT b'1' COMMENT '启停状态 1启用 0停用',
  `number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `department_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门编号',
  `sex` int NOT NULL DEFAULT '0' COMMENT '性别 0->默认，1->男，2->女，3->保密',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of cms_leader
-- ----------------------------
INSERT INTO `cms_leader` VALUES ('1504855041217961986', '', '0001', '燕双鹰', '002', '1', '13666666666', '\0', '2022-03-19 00:19:35', '2022-03-19 00:19:35');

-- ----------------------------
-- Table structure for cms_leave
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cms_leave` (
  `id` bigint NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请假人姓名',
  `user_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请假人编号',
  `meeting_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '会议名称',
  `meeting_id` bigint NOT NULL COMMENT '会议id',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请假理由',
  `status` int NOT NULL DEFAULT '0' COMMENT '请假状态 0审核中 1通过 2拒绝',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of cms_leave
-- ----------------------------

-- ----------------------------
-- Table structure for cms_meeting
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cms_meeting` (
  `id` bigint NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '会议名称',
  `describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '会议描述',
  `user_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发起人编号',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发起人姓名',
  `member_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '会议成员编号',
  `type` int NOT NULL DEFAULT '0' COMMENT '0线上 1线下',
  `room_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会议室编号',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `time_frame` int NOT NULL,
  `meeting_status` int NOT NULL DEFAULT '0' COMMENT '会议状态 0审核中 1已通过 2未通过 3已开始 4已结束',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of cms_meeting
-- ----------------------------

-- ----------------------------
-- Table structure for cms_room
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cms_room` (
  `id` bigint NOT NULL,
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '会议室编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '会议室名称',
  `size` int NOT NULL COMMENT '容纳人数',
  `status` int NOT NULL DEFAULT '1' COMMENT '状态 0禁用 1启用',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of cms_room
-- ----------------------------
INSERT INTO `cms_room` VALUES ('1511534301173297153', '105', '会议室1', '20', '1', '\0', '2022-04-06 10:40:35', '2022-04-06 10:40:35');
INSERT INTO `cms_room` VALUES ('1518423362912362497', '106', '会议室2', '20', '1', '\0', '2022-04-25 10:55:14', '2022-04-25 10:55:14');
INSERT INTO `cms_room` VALUES ('1537360128225841153', '107', '会议室3', '30', '1', '\0', '2022-06-16 17:03:12', '2022-06-16 17:03:12');

-- ----------------------------
-- Table structure for cms_staff
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cms_staff` (
  `id` bigint NOT NULL,
  `is_enable` bit(1) NOT NULL DEFAULT b'1' COMMENT '启停状态 1启用 0停用',
  `number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `department_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门编号',
  `sex` int NOT NULL DEFAULT '0' COMMENT '性别 0->默认，1->男，2->女，3->保密',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of cms_staff
-- ----------------------------
INSERT INTO `cms_staff` VALUES ('1538054532355854337', '', '0016', '蒋卓航', '002', '1', '15867198331', '\0', '2022-06-18 15:02:30', '2022-06-18 15:02:30');
INSERT INTO `cms_staff` VALUES ('1538059338562670593', '', '0012', '钟和晴', '002', '1', '15007040607', '\0', '2022-06-18 15:21:36', '2022-06-18 15:21:36');

-- ----------------------------
-- Table structure for message
-- ----------------------------
CREATE TABLE IF NOT EXISTS `message` (
  `id` bigint NOT NULL COMMENT 'id',
  `send_number` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '发送端编号',
  `send_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发送者姓名',
  `accept_number` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '接收端编号',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '内容 ',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '类型  （用户私信   系统通知   被评论回复等消息通知）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `is_read` bit(1) NOT NULL DEFAULT b'0' COMMENT '状态 0--未读  1-- 已读',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `key_number` (`send_number`) USING BTREE,
  KEY `number` (`accept_number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci ROW_FORMAT=DYNAMIC COMMENT='消息表';

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for sys_audit_log
-- ----------------------------
CREATE TABLE IF NOT EXISTS `sys_audit_log` (
  `id` bigint NOT NULL COMMENT '编号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '业务名称',
  `type` int DEFAULT '10' COMMENT '日志类型 10:操作日志 20:错误日志',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '请求IP',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '请求资源路径',
  `req_method` varchar(255) NOT NULL DEFAULT '' COMMENT '请求方式',
  `req_param` text COMMENT '请求参数',
  `res_param` longtext COMMENT '返回参数',
  `exception` text COMMENT '异常信息',
  `timeout` varchar(255) NOT NULL DEFAULT '' COMMENT '耗时',
  `user_id` bigint DEFAULT NULL COMMENT '用户唯一编号',
  `user_name` varchar(255) DEFAULT '' COMMENT '用户姓名',
  `create_user` varchar(20) DEFAULT '' COMMENT '创建用户',
  `update_user` varchar(20) DEFAULT '' COMMENT '更新用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT COMMENT='访问日志信息 ';

-- ----------------------------
-- Records of sys_audit_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
CREATE TABLE IF NOT EXISTS `sys_config` (
  `id` bigint NOT NULL COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '创建用户',
  `update_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '更新用户',
  `lng` decimal(10,7) DEFAULT NULL COMMENT '经度',
  `lat` decimal(10,7) DEFAULT NULL COMMENT '纬度',
  `radius` double(10,0) DEFAULT NULL COMMENT '签到半径(米)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='签到表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1510048178106359810', '签到配置', '2022-04-06 18:55:19', '2022-04-06 18:55:24', '\0', '', '', '119.6365470', '29.0631180', '500');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
CREATE TABLE IF NOT EXISTS `sys_permission` (
  `id` bigint NOT NULL COMMENT '权限ID 权限ID',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间  ',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间  ',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否禁用 0->可用，1->禁用',
  `parent_id` bigint NOT NULL DEFAULT '0' COMMENT '父级权限ID 父级权限ID',
  `permission_name` varchar(32) DEFAULT '' COMMENT '权限名称 中文',
  `value` varchar(128) NOT NULL DEFAULT '' COMMENT '权限编码 ',
  `type` int NOT NULL DEFAULT '0' COMMENT '权限类型 权限类型：0->目录；1->菜单；2->按钮（接口绑定权限）',
  `path` varchar(100) NOT NULL DEFAULT '' COMMENT ' 前端路由路径',
  `component` varchar(255) NOT NULL DEFAULT '' COMMENT '组件路由',
  `ban` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否被禁用 （0:禁止,1:正常)',
  `sort` int NOT NULL DEFAULT '0' COMMENT '菜单排序',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `label` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限名称 英文',
  `create_user` varchar(20) DEFAULT '' COMMENT '创建用户',
  `update_user` varchar(20) DEFAULT '' COMMENT '更新用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT COMMENT='cms_permission ';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('1511947273994117122', '2022-04-07 14:01:33', '2022-04-07 14:01:33', '\0', '0', '', '', '0', '/permission', '', '', '100', 'iconfont icon-xitongguanli', '系统管理', '', '');
INSERT INTO `sys_permission` VALUES ('1511947851855962113', '2022-04-07 14:03:51', '2022-04-07 14:03:51', '\0', '1511947273994117122', '', '', '1', 'user', 'views/permission/user', '', '99', 'iconfont icon-yonghuguanli', '用户管理', '', '');
INSERT INTO `sys_permission` VALUES ('1511950095481442306', '2022-04-07 14:12:46', '2022-04-07 14:12:46', '\0', '1511947273994117122', '', '', '1', 'role', 'views/permission/role', '', '98', 'iconfont icon-jiaoseguanli2', '角色管理', '', '');
INSERT INTO `sys_permission` VALUES ('1511950334045065218', '2022-04-07 14:13:43', '2022-04-07 14:13:43', '\0', '1511947273994117122', '', '', '1', 'department', 'views/permission/department', '', '94', 'iconfont icon-bumenguanli', '部门管理', '', '');
INSERT INTO `sys_permission` VALUES ('1511950530334298113', '2022-04-07 14:14:30', '2022-04-07 14:14:30', '\0', '1511947273994117122', '', '', '1', 'meetingroom', 'views/permission/meetingroom', '', '95', 'iconfont icon-hangzhengguanli-huiyishiguanli', '会议室管理', '', '');
INSERT INTO `sys_permission` VALUES ('1511950860157587458', '2022-04-07 14:15:48', '2022-04-07 14:15:48', '\0', '0', '', '', '0', '/meeting', '', '', '80', 'iconfont icon-huiyiguanli1', '会议管理', '', '');
INSERT INTO `sys_permission` VALUES ('1511951169076465665', '2022-04-07 14:17:02', '2022-04-07 14:17:02', '\0', '1511950860157587458', '', '', '1', 'record', 'views/meeting/record', '', '79', 'iconfont icon-huiyijilu', '会议记录', '', '');
INSERT INTO `sys_permission` VALUES ('1511951424094343169', '2022-04-07 14:18:03', '2022-04-07 14:18:03', '\0', '1511950860157587458', '', '', '1', 'attendance', 'views/meeting/attendance', '', '78', 'iconfont icon-huiyikaoqin', '会议考勤', '', '');
INSERT INTO `sys_permission` VALUES ('1511951660493705218', '2022-04-07 14:18:59', '2022-04-07 14:18:59', '\0', '1511950860157587458', '', '', '1', 'holiday', 'views/meeting/holiday', '', '77', 'iconfont icon-SUI_shenhetongzhi', '会议请假', '', '');
INSERT INTO `sys_permission` VALUES ('1511951965763538945', '2022-04-07 14:20:12', '2022-04-07 14:20:12', '', '0', '', '', '0', '/message', '', '', '60', 'iconfont icon-xiaoxiguanli', '消息管理', '', '');
INSERT INTO `sys_permission` VALUES ('1511952270798491650', '2022-04-07 14:21:25', '2022-04-07 14:21:25', '', '1511951965763538945', '', '', '1', 'meetingMessage', 'views/message/meetingMessage', '', '59', 'iconfont icon-mingpianjiaxiaochengxuicon_tongzhi', '会议审核通知', '', '');
INSERT INTO `sys_permission` VALUES ('1511952415371956226', '2022-04-07 14:21:59', '2022-04-07 14:21:59', '', '1511951965763538945', '', '', '1', 'holidayMessage', 'views/message/holidayMessage', '', '58', 'iconfont icon-qingjiashenhe1', '请假审核通知', '', '');
INSERT INTO `sys_permission` VALUES ('1511952551078662145', '2022-04-07 14:22:32', '2022-04-07 14:22:32', '', '1511951965763538945', '', '', '1', 'attendMessage', 'views/message/attendMessage', '', '57', 'iconfont icon-tongzhi2', '参会通知', '', '');
INSERT INTO `sys_permission` VALUES ('1512318224137080833', '2022-04-08 14:35:36', '2022-04-08 14:35:36', '\0', '1511947851855962113', '', 'per_user_add', '2', '', '', '', '0', '', '添加用户信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512318416395587585', '2022-04-08 14:36:22', '2022-04-08 14:36:22', '\0', '1511947851855962113', '', 'per_user_edit', '2', '', '', '', '0', '', '修改用户信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512318479645691905', '2022-04-08 14:36:37', '2022-04-08 14:36:37', '\0', '1511947851855962113', '', 'per_user_delete', '2', '', '', '', '0', '', '删除用户信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512318555688423426', '2022-04-08 14:36:55', '2022-04-08 14:36:55', '\0', '1511947851855962113', '', 'per_user_view', '2', '', '', '', '0', '', '查看用户信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512319789656854529', '2022-04-08 14:41:49', '2022-04-08 14:41:49', '\0', '1511950095481442306', '', 'per_role_delete', '2', '', '', '', '0', '', '删除角色信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512319905981681665', '2022-04-08 14:42:17', '2022-04-08 14:42:17', '\0', '1511950095481442306', '', 'per_role_add', '2', '', '', '', '0', '', '添加角色信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512319954547527681', '2022-04-08 14:42:28', '2022-04-08 14:42:28', '\0', '1511950095481442306', '', 'per_role_edit', '2', '', '', '', '0', '', '修改角色信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512320007815188482', '2022-04-08 14:42:41', '2022-04-08 14:42:41', '\0', '1511950095481442306', '', 'per_role_view', '2', '', '', '', '0', '', '查看角色信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512320320395694081', '2022-04-08 14:43:56', '2022-04-08 14:43:56', '\0', '1511950334045065218', '', 'per_department_add', '2', '', '', '', '0', '', '添加部门信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512320371528454145', '2022-04-08 14:44:08', '2022-04-08 14:44:08', '\0', '1511950334045065218', '', 'per_department_delete', '2', '', '', '', '0', '', '删除部门信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512320441959206913', '2022-04-08 14:44:25', '2022-04-08 14:44:25', '\0', '1511950334045065218', '', 'per_department_edit', '2', '', '', '', '0', '', '修改部门信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512320485001154561', '2022-04-08 14:44:35', '2022-04-08 14:44:35', '\0', '1511950334045065218', '', 'per_department_view', '2', '', '', '', '0', '', '查看部门信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512320892523925506', '2022-04-08 14:46:12', '2022-04-08 14:46:12', '\0', '1511950530334298113', '', 'per_meetingroom_add', '2', '', '', '', '0', '', '添加会议室信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512320938564800513', '2022-04-08 14:46:23', '2022-04-08 14:46:23', '\0', '1511950530334298113', '', 'per_meetingroom_delete', '2', '', '', '', '0', '', '删除会议室信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512321006424444930', '2022-04-08 14:46:39', '2022-04-08 14:46:39', '\0', '1511950530334298113', '', 'per_meetingroom_edit', '2', '', '', '', '0', '', '修改会议室信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512321062141579265', '2022-04-08 14:46:52', '2022-04-08 14:46:52', '\0', '1511950530334298113', '', 'per_meetingroom_view', '2', '', '', '', '0', '', '查看会议室信息', '', '');
INSERT INTO `sys_permission` VALUES ('1512341250534690818', '2022-04-08 16:07:06', '2022-04-08 16:07:06', '\0', '1511947273994117122', '', '', '1', 'menu', 'views/permission/menu', '', '92', 'iconfont icon-caidan3', '菜单管理', '', '');
INSERT INTO `sys_permission` VALUES ('1513324593187377153', '2022-04-11 09:14:34', '2022-04-11 09:14:34', '\0', '1512341250534690818', '', 'per_menu_add', '2', '', '', '', '0', null, '添加菜单信息', '', '');
INSERT INTO `sys_permission` VALUES ('1513325296492466178', '2022-04-11 09:17:21', '2022-04-11 09:17:21', '\0', '1512341250534690818', '', 'per_menu_delete', '2', '', '', '', '0', null, '删除菜单信息', '', '');
INSERT INTO `sys_permission` VALUES ('1513326576807956481', '2022-04-11 09:22:27', '2022-04-11 09:22:27', '\0', '1512341250534690818', '', 'per_menu_edit', '2', '', '', '', '0', null, '修改菜单信息', '', '');
INSERT INTO `sys_permission` VALUES ('1513684326616387585', '2022-04-12 09:04:00', '2022-04-12 09:04:00', '\0', '1511951169076465665', '', 'per_record_delete', '2', '', '', '', '0', null, '删除会议记录', '', '');
INSERT INTO `sys_permission` VALUES ('1513684483655323650', '2022-04-12 09:04:37', '2022-04-12 09:04:37', '\0', '1511951169076465665', '', 'per_record_edit', '2', '', '', '', '0', null, '修改会议记录', '', '');
INSERT INTO `sys_permission` VALUES ('1513687668046417922', '2022-04-12 09:17:16', '2022-04-12 09:17:16', '\0', '1511951169076465665', '', 'per_record_view', '2', '', '', '', '0', null, '查看会议记录', '', '');
INSERT INTO `sys_permission` VALUES ('1513701101567369218', '2022-04-12 10:10:39', '2022-04-12 10:10:39', '\0', '1511951424094343169', '', 'per_attendance_view', '2', '', '', '', '0', null, '查看会议考勤记录', '', '');
INSERT INTO `sys_permission` VALUES ('1513701215761489922', '2022-04-12 10:11:06', '2022-04-12 10:11:06', '\0', '1511951424094343169', '', 'per_attendance_edit', '2', '', '', '', '0', null, '修改会议考勤记录', '', '');
INSERT INTO `sys_permission` VALUES ('1513701397530042369', '2022-04-12 10:11:50', '2022-04-12 10:11:50', '\0', '1511951424094343169', '', 'per_attendance_delete', '2', '', '', '', '0', null, '删除会议考勤记录', '', '');
INSERT INTO `sys_permission` VALUES ('1513702660980236290', '2022-04-12 10:16:51', '2022-04-12 10:16:51', '\0', '1511951169076465665', '', 'per_record_through', '2', '', '', '', '0', null, '通过审核', '', '');
INSERT INTO `sys_permission` VALUES ('1513702901804589057', '2022-04-12 10:17:48', '2022-04-12 10:17:48', '\0', '1511951169076465665', '', 'per_record_back', '2', '', '', '', '0', null, '打回审核', '', '');
INSERT INTO `sys_permission` VALUES ('1513710798462672897', '2022-04-12 10:49:11', '2022-04-12 10:49:11', '\0', '1511951660493705218', '', 'per_holiday_delete', '2', '', '', '', '0', null, '删除请假记录', '', '');
INSERT INTO `sys_permission` VALUES ('1513710889411960834', '2022-04-12 10:49:33', '2022-04-12 10:49:33', '\0', '1511951660493705218', '', 'per_holiday_edit', '2', '', '', '', '0', null, '修改请假记录', '', '');
INSERT INTO `sys_permission` VALUES ('1513710981468545025', '2022-04-12 10:49:55', '2022-04-12 10:49:55', '\0', '1511951660493705218', '', 'per_holiday_view', '2', '', '', '', '0', null, '查看请假记录', '', '');
INSERT INTO `sys_permission` VALUES ('1519239304994463746', '2022-04-27 16:57:31', '2022-04-27 16:57:31', '', '0', '', '', '1', 'leader', '/view/permission/leader', '', '6', 'iconfont icon-yonghuguanli3', '领导管理', '', '');
INSERT INTO `sys_permission` VALUES ('1519239719576248321', '2022-04-27 16:59:09', '2022-04-27 16:59:09', '\0', '1511947273994117122', '', '', '1', 'leader', 'views/permission/leader', '', '97', 'iconfont icon-huiyiguanli_1', '领导管理', '', '');
INSERT INTO `sys_permission` VALUES ('1519240923429576705', '2022-04-27 17:03:56', '2022-04-27 17:03:56', '\0', '1511947273994117122', '', '', '1', 'employee', 'views/permission/employee', '', '96', 'iconfont icon-yonghuguanli3', '员工管理', '', '');
INSERT INTO `sys_permission` VALUES ('1519241777293402113', '2022-04-27 17:07:20', '2022-04-27 17:07:20', '\0', '1519239719576248321', '', 'per_leader_add', '2', '', '', '', '0', null, '添加领导信息', '', '');
INSERT INTO `sys_permission` VALUES ('1519241939629744130', '2022-04-27 17:07:59', '2022-04-27 17:07:59', '\0', '1519239719576248321', '', 'per_leader_delete', '2', '', '', '', '0', null, '删除领导信息', '', '');
INSERT INTO `sys_permission` VALUES ('1519244234853556225', '2022-04-27 17:17:06', '2022-04-27 17:17:06', '\0', '1519239719576248321', '', 'per_leader_edit', '2', '', '', '', '0', null, '修改领导信息', '', '');
INSERT INTO `sys_permission` VALUES ('1519244358350643201', '2022-04-27 17:17:35', '2022-04-27 17:17:35', '\0', '1519239719576248321', '', 'per_leader_view', '2', '', '', '', '0', null, '查看领导信息', '', '');
INSERT INTO `sys_permission` VALUES ('1519244834911657986', '2022-04-27 17:19:29', '2022-04-27 17:19:29', '\0', '1519240923429576705', '', 'per_employee_add', '2', '', '', '', '0', null, '添加员工信息', '', '');
INSERT INTO `sys_permission` VALUES ('1519244923465998338', '2022-04-27 17:19:50', '2022-04-27 17:19:50', '\0', '1519240923429576705', '', 'per_employee_delete', '2', '', '', '', '0', null, '删除员工信息', '', '');
INSERT INTO `sys_permission` VALUES ('1519245004021800962', '2022-04-27 17:20:09', '2022-04-27 17:20:09', '\0', '1519240923429576705', '', 'per_employee_edit', '2', '', '', '', '0', null, '修改员工信息', '', '');
INSERT INTO `sys_permission` VALUES ('1519245081847111682', '2022-04-27 17:20:28', '2022-04-27 17:20:28', '\0', '1519240923429576705', '', 'per_employee_view', '2', '', '', '', '0', null, '查看员工信息', '', '');
INSERT INTO `sys_permission` VALUES ('1519960243600101377', '2022-04-29 16:42:16', '2022-04-29 16:42:16', '\0', '0', '', '', '0', '/signconfig', 'views/signconfig/signconfig', '', '50', 'iconfont icon-xitongguanli2', '签到配置', '', '');
INSERT INTO `sys_permission` VALUES ('1537355083916775426', '2022-06-16 16:43:09', '2022-06-16 16:43:09', '\0', '1511947273994117122', '', '', '1', 'face', 'views/permission/face', '', '93', 'iconfont icon-jiaoseguanli1', '人脸管理', '', '');
INSERT INTO `sys_permission` VALUES ('1537355339567992833', '2022-06-16 16:44:10', '2022-06-16 16:44:10', '\0', '1537355083916775426', '', 'per_face_delete', '2', '', '', '', '0', null, '删除人脸信息', '', '');
INSERT INTO `sys_permission` VALUES ('1538055754240495617', '2022-06-18 15:07:22', '2022-06-18 15:07:22', '\0', '1511947851855962113', '', 'per_user_resetpassword', '2', '', '', '', '0', null, '重置密码', '', '');
INSERT INTO `sys_permission` VALUES ('1538061930256666626', '2022-06-18 15:31:54', '2022-06-18 15:31:54', '\0', '1511950095481442306', '', 'per_role_distribution', '2', '', '', '', '0', null, '分配权限', '', '');
INSERT INTO `sys_permission` VALUES ('1538062104894902273', '2022-06-18 15:32:36', '2022-06-18 15:32:36', '\0', '1511950095481442306', '', 'per_role_permission', '2', '', '', '', '0', null, '分配角色权限', '', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
CREATE TABLE IF NOT EXISTS `sys_role` (
  `id` bigint NOT NULL COMMENT ' 角色ID',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除 0->未删除，1->删除',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '角色中文名称',
  `auth_data` text COMMENT '权限码 权限编码',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '角色说明',
  `create_user` varchar(20) DEFAULT '' COMMENT '创建用户',
  `update_user` varchar(20) DEFAULT '' COMMENT '更新用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT COMMENT='cms_role ';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1504855093013422082', '2022-03-19 00:19:47', '2022-03-19 00:19:47', '\0', '领导层', '{\"authList\":[\"1519240923429576705\",\"1519244834911657986\",\"1519244923465998338\",\"1519245004021800962\",\"1519245081847111682\",\"1511950530334298113\",\"1512320892523925506\",\"1512320938564800513\",\"1512321006424444930\",\"1512321062141579265\",\"1511950334045065218\",\"1512320441959206913\",\"1512320320395694081\",\"1512320371528454145\",\"1512320485001154561\",\"1511950860157587458\",\"1511951169076465665\",\"1513702901804589057\",\"1513702660980236290\",\"1513687668046417922\",\"1513684326616387585\",\"1513684483655323650\",\"1511951424094343169\",\"1513701101567369218\",\"1513701397530042369\",\"1513701215761489922\",\"1511951660493705218\",\"1513710889411960834\",\"1513710798462672897\",\"1513710981468545025\",\"1519960243600101377\",\"1511947273994117122\"]}', '领导层', '', '');
INSERT INTO `sys_role` VALUES ('1504855146679541761', '2022-03-19 00:20:00', '2022-03-19 00:20:00', '\0', '普通员工', '{\"authList\":[]}', '普通员工', '', '');
INSERT INTO `sys_role` VALUES ('1512239654433923074', '2022-04-08 09:23:23', '2022-04-08 09:23:23', '\0', '超级管理员', '{\"authList\":[\"1511947273994117122\",\"1511947851855962113\",\"1512318224137080833\",\"1512318416395587585\",\"1512318479645691905\",\"1512318555688423426\",\"1511950095481442306\",\"1512319789656854529\",\"1512319905981681665\",\"1512319954547527681\",\"1512320007815188482\",\"1519239719576248321\",\"1519241777293402113\",\"1519241939629744130\",\"1519244234853556225\",\"1519244358350643201\",\"1519240923429576705\",\"1519244834911657986\",\"1519244923465998338\",\"1519245004021800962\",\"1519245081847111682\",\"1511950530334298113\",\"1512320892523925506\",\"1512320938564800513\",\"1512321006424444930\",\"1512321062141579265\",\"1511950334045065218\",\"1512320441959206913\",\"1512320320395694081\",\"1512320371528454145\",\"1512320485001154561\",\"1512341250534690818\",\"1513324593187377153\",\"1513325296492466178\",\"1513326576807956481\",\"1537355083916775426\",\"1537355339567992833\",\"1511950860157587458\",\"1511951169076465665\",\"1513702901804589057\",\"1513702660980236290\",\"1513687668046417922\",\"1513684326616387585\",\"1513684483655323650\",\"1511951424094343169\",\"1513701101567369218\",\"1513701397530042369\",\"1513701215761489922\",\"1511951660493705218\",\"1513710889411960834\",\"1513710798462672897\",\"1513710981468545025\",\"1519960243600101377\",\"1538055754240495617\",\"1538061930256666626\",\"1538062104894902273\"]}', '超级管理员', '', '');
INSERT INTO `sys_role` VALUES ('1538064736493506562', '2022-06-18 15:43:03', '2022-06-18 15:43:03', '', '第三章', null, '是', '', '');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` bigint NOT NULL COMMENT 'id',
  `number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户编号',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `type` int NOT NULL DEFAULT '10' COMMENT '身份类型 10->普通员工，20->管理层 30系统管理员 默认普通员工',
  `department_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门编号',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `sex` int NOT NULL DEFAULT '0' COMMENT '0默认1男2女3保密',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `auth_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '权限集合',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_user` (`number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT COMMENT='用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1504855198848294914', '0001', '燕双鹰', '30', '002', '1366666666', '1', '$2a$10$Gl2wIMG7anilXxgnGnUAeOso3vGMPq7R6nNCaRVLkfSO2ei5iRBGS', '{\"authList\":[\"1504855093013422082\",\"1504855146679541761\",\"1512239654433923074\"]}', '\0', '2022-03-19 00:20:12', '2022-03-19 00:20:12');
INSERT INTO `sys_user` VALUES ('1538058240762318849', '0016', '蒋卓航', '10', '002', '15867198331', '1', '$2a$10$CN.GvaFsJmNvmE7xwdsb8uf1DSIqBytMxgvp2zdwkmQsQ6k7Ibiaa', '{\"authList\":[\"1504855146679541761\"]}', '\0', '2022-06-18 15:17:15', '2022-06-18 15:17:15');
INSERT INTO `sys_user` VALUES ('1538059351044919298', '0012', '钟和晴', '10', '002', '15007040607', '1', '$2a$10$nLHw7nwEnZonYE2exiukF.lYxr78esRl7CZqdOHbx7/TD.BhTZyqy', '{\"authList\":[\"1504855146679541761\"]}', '\0', '2022-06-18 15:21:39', '2022-06-18 15:21:39');
