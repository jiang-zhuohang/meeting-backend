package com.jhc.BackstageController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsStaffAddDto;
import com.jhc.DTO.CmsStaffDto;
import com.jhc.DTO.CmsStaffUpdateDto;
import com.jhc.VO.CmsStaffVo;
import com.jhc.service.ICmsStaffService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jiangzhuohang
 * @create 2022/3/18 19:51
 */
@RestController
@RequestMapping("/backstageCmsStaff")
@Api(tags="系统 基础数据接口")
public class BackstageCmsStaffController {
    @Autowired
    private ICmsStaffService iCmsStaffService;

    @ApiOperation("获取普通员工成员,分页条件查询")
    @GetMapping("/cmsStaffPageData")
    public CommonResult cdmTeachPageData(CmsStaffDto pageVo) {
        IPage<CmsStaffVo> cmsStaffPageData = iCmsStaffService.getCmsStaffPageData(pageVo);
        return CommonResult.success(cmsStaffPageData);
    }


    @ApiOperation("增加普通员工")
    @PostMapping("/addCmsStaff")
    public CommonResult addCmsStaff(@RequestBody CmsStaffAddDto staffAddDto) {
        return iCmsStaffService.addCmsStaff(staffAddDto);
    }


    @ApiOperation("删除普通员工成员(批量)")
    @DeleteMapping("/deleteCmsStaff")
    public CommonResult deleteCmsStaff(@RequestBody List<Long> id) {
        return iCmsStaffService.deleteCmsStaff(id);
    }


    @ApiOperation("更改普通员工成员")
    @PutMapping("/UpdateCmsStaff")
    public CommonResult UpdateCmsStaff(@RequestBody CmsStaffUpdateDto updateStaff) {
        return iCmsStaffService.updateCmsStaff(updateStaff);
    }


    @GetMapping("/staffList")
    @ApiOperation("获取普通员工下拉框")
    public CommonResult  getStaffList(){
        return CommonResult.success(iCmsStaffService.getStaffList());
    }

}

