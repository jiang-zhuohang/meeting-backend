package com.jhc.BackstageController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsLeaderAddDto;
import com.jhc.DTO.CmsLeaderDto;
import com.jhc.DTO.CmsLeaderUpdateDto;
import com.jhc.VO.CmsLeaderVo;
import com.jhc.service.ICmsLeaderService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jiangzhuohang
 * @create 2022/3/18 19:51
 */
@RestController
@RequestMapping("/backstageCmsLeader")
@Api(tags="系统 基础数据接口")
public class BackstageCmsLeaderController {
    @Autowired
    private ICmsLeaderService iCmsLeaderService;

    @ApiOperation("获取领导层成员,分页条件查询")
    @GetMapping("/cmsLeaderPageData")
    public CommonResult cdmTeachPageData(CmsLeaderDto pageVo) {
        IPage<CmsLeaderVo> cmsLeaderPageData = iCmsLeaderService.getCmsLeaderPageData(pageVo);
        return CommonResult.success(cmsLeaderPageData);
    }


    @ApiOperation("增加领导层成员")
    @PostMapping("/addCmsLeader")
    public CommonResult addCmsLeader(@RequestBody CmsLeaderAddDto leaderAddDto) {
        return iCmsLeaderService.addCmsLeader(leaderAddDto);
    }


    @ApiOperation("删除领导层成员(批量)")
    @DeleteMapping("/deleteCmsLeader")
    public CommonResult deleteCmsLeader(@RequestBody List<Long> id) {
        return iCmsLeaderService.deleteCmsLeader(id);
    }


    @ApiOperation("更改领导层成员")
    @PutMapping("/UpdateCmsLeader")
    public CommonResult UpdateCmsLeader(@RequestBody CmsLeaderUpdateDto updateLeader) {
        return iCmsLeaderService.updateCmsLeader(updateLeader);
    }


    @GetMapping("/leaderList")
    @ApiOperation("获取领导层下拉框")
    public CommonResult  getLeaderList(){
        return CommonResult.success(iCmsLeaderService.getLeaderList());
    }

}

