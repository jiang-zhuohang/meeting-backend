package com.jhc.BackstageController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsRoomAddDto;
import com.jhc.DTO.CmsRoomDto;
import com.jhc.DTO.CmsRoomUpdateDto;
import com.jhc.VO.CmsRoomVo;
import com.jhc.service.ICmsRoomService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 18:51
 */
@RestController
@RequestMapping("/backstageCmsRoom")
@Api(tags = "系统 基础数据接口")
public class BackstageCmsRoomController {
    @Autowired
    private ICmsRoomService roomService;

    @ApiOperation("后台获取会议室列表 ")
    @GetMapping("/getCmsRoom")
    public CommonResult getRooms(CmsRoomDto pageVo){
        IPage<CmsRoomVo> cmsRoomPageData = roomService.getCmsRoomPageData(pageVo);
        return CommonResult.success(cmsRoomPageData);
    }

    @PostMapping("addCmsRoom")
    @ApiOperation("添加会议室 ")
    public CommonResult addCmsRoom(@RequestBody CmsRoomAddDto dto) {
        return roomService.addCmsRoom(dto);
    }

    @DeleteMapping("/deleteCmsRoomById")
    @ApiOperation("通过会议室 id批量删除会议室 ")
    public CommonResult deleteCmsRoomById(@RequestBody List<Long> list) {
        return  roomService.deleteCmsRoom(list);
    }
    @PutMapping("/updateCmsRoomById")
    @ApiOperation("通过会议室id修改会议室 ")
    public CommonResult updateCmsRoomById(@RequestBody CmsRoomUpdateDto updateDto) {
        return roomService.updateCmsRoom(updateDto);
    }
}
