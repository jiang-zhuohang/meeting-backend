package com.jhc.BackstageController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.BasePage;
import com.jhc.VO.FaceInfoVo;
import com.jhc.service.ICmsFaceService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 18:51
 */
@RestController
@RequestMapping("/backstageCmsFace")
@Api(tags = "人脸数据接口")
public class BackstageCmsFaceController {
    @Autowired
    private ICmsFaceService faceService;

    /**
     * 启动项目时检测并初始化向量引擎库
     */
    @PostConstruct
    public void createEngine(){
        System.out.println(faceService.createEngine().getMessage());
    }

    @ApiOperation("后台人脸数据列表 ")
    @GetMapping("/getCmsFace")
    public CommonResult getFaces(BasePage pageVo){
        IPage<FaceInfoVo> cmsFacePageData = faceService.getCmsFacePageData(pageVo);
        return CommonResult.success(cmsFacePageData);
    }

    @DeleteMapping("/deleteCmsFaceById")
    @ApiOperation("通过人脸 id批量删除人脸 ")
    public CommonResult deleteCmsFaceById(@RequestBody List<Long> list) {
        return  faceService.deleteCmsFaceById(list);
    }

}
