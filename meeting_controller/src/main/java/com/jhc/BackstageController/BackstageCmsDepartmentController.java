package com.jhc.BackstageController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsDepartmentAddDto;
import com.jhc.DTO.CmsDepartmentDto;
import com.jhc.DTO.CmsDepartmentUpdateDto;
import com.jhc.VO.CmsDepartmentVo;
import com.jhc.service.ICmsDepartmentService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jiangzhuohang
 * @create 2022/4/5 23:48
 */
@RestController
@RequestMapping("/backstageCmsDepartment")
@Api(tags="系统 基础数据接口")
public class BackstageCmsDepartmentController {
    @Autowired
    private ICmsDepartmentService iCmsDepartmentService;

    @ApiOperation("后台获取部门列表 ")
    @GetMapping("/getCmsDepartment")
    public CommonResult getDepartments(CmsDepartmentDto pageVo){
        IPage<CmsDepartmentVo> cmsDepartmentPageData = iCmsDepartmentService.getCmsDepartmentPageData(pageVo);
        return CommonResult.success(cmsDepartmentPageData);
    }

    @GetMapping("/departmentList")
    @ApiOperation("后台获取部门下拉框(部门名,编号)")
    public CommonResult  getDepartmentList(){
        return CommonResult.success(iCmsDepartmentService.getBasDepartmentList());
    }

    @PostMapping("addCmsDepartment")
    @ApiOperation("添加部门 ")
    public CommonResult addCmsDepartment(@RequestBody CmsDepartmentAddDto dto) {
        return iCmsDepartmentService.addCmsDepartment(dto);
    }

    @DeleteMapping("/deleteCmsDepartmentById")
    @ApiOperation("通过部门 id批量删除部门 ")
    public CommonResult deleteCmsDepartmentById(@RequestBody List<Long> list) {
        return  iCmsDepartmentService.deleteCmsDepartment(list);
    }
    @PutMapping("/updateCmsDepartmentById")
    @ApiOperation("通过部门id修改部门 ")
    public CommonResult updateCmsDepartmentById(@RequestBody CmsDepartmentUpdateDto updateDto) {
        return iCmsDepartmentService.updateCmsDepartment(updateDto);
    }
}
