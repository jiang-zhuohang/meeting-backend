package com.jhc.BackstageController;

import com.jhc.DTO.SysConfigAddDto;
import com.jhc.DTO.SysConfigUpdateDto;
import com.jhc.service.ISysConfigService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 18:56
 */@RestController
@RequestMapping("/sysConfig")
@Api(tags = "系统 签到设置接口")
public class BackstageSysConfigController {
    @Autowired
    private ISysConfigService iSysConfigService;

    @GetMapping("getSysConfig")
    @ApiOperation("获取签到配置 ")
    public CommonResult getSysConfig() {
        return iSysConfigService.getConfig();
    }

    @PostMapping("addSysConfig")
    @ApiOperation("添加签到配置 ")
    public CommonResult addSysConfig(@RequestBody SysConfigAddDto dto) {
        return iSysConfigService.addConfig(dto);
    }


    @PutMapping("updateSysConfig")
    @ApiOperation("更新签到配置 ")
    public CommonResult updateSysConfig(@RequestBody SysConfigUpdateDto dto) {
        return iSysConfigService.updateConfig(dto);
    }

}
