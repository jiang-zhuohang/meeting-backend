package com.jhc.BackstageController;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsAttendanceAddDto;
import com.jhc.DTO.CmsAttendanceDto;
import com.jhc.DTO.CmsAttendanceUpdateDto;
import com.jhc.VO.CmsAttendanceVo;
import com.jhc.service.ICmsAttendanceService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@RestController
@RequestMapping("/backstageCmsAttendance")
@Api(tags="考勤接口")
public class BackstageCmsAttendanceController {

    @Autowired
    private ICmsAttendanceService iCmsAttendanceService;

    @ApiOperation("获取考勤列表 ")
    @GetMapping("/getBasCmsAttendance")
    public CommonResult getAttendances(CmsAttendanceDto pageVo){
        IPage<CmsAttendanceVo> cmsAttendancePageData = iCmsAttendanceService.getCmsAttendancePageData(pageVo);
        return CommonResult.success(cmsAttendancePageData);
    }

    @PostMapping("addBasCmsAttendance")
    @ApiOperation("添加考勤 ")
    public CommonResult addCmsAttendance(@RequestBody CmsAttendanceAddDto dto) {
        return iCmsAttendanceService.addCmsAttendance(dto);
    }

    @DeleteMapping("/deleteBasCmsAttendanceById")
    @ApiOperation("通过考勤 id批量删除考勤 ")
    public CommonResult deleteCmsAttendanceById(@RequestBody List<Long> list) {
        return  iCmsAttendanceService.deleteCmsAttendance(list);
    }
    @PutMapping("/updateBasCmsAttendanceById")
    @ApiOperation("通过考勤id修改考勤 ")
    public CommonResult updateCmsAttendanceById(@RequestBody CmsAttendanceUpdateDto updateDto) {
        return iCmsAttendanceService.updateCmsAttendance(updateDto);
    }
}

