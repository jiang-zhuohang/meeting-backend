package com.jhc.BackstageController;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsMeetingAddDto;
import com.jhc.DTO.CmsMeetingDto;
import com.jhc.DTO.CmsMeetingUpdateDto;
import com.jhc.VO.CmsMeetingVo;
import com.jhc.service.ICmsMeetingService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@RestController
@RequestMapping("/backstageCmsMeeting")
@Api(tags="会议接口")
public class BackstageCmsMeetingController {

    @Autowired
    private ICmsMeetingService iCmsMeetingService;

    @ApiOperation("后台获取会议列表 ")
    @GetMapping("/getCmsMeeting")
    public CommonResult getMeetings(CmsMeetingDto pageVo){
        IPage<CmsMeetingVo> cmsMeetingPageData = iCmsMeetingService.getCmsMeetingPageData(pageVo);
        return CommonResult.success(cmsMeetingPageData);
    }

    @PostMapping("addCmsMeeting")
    @ApiOperation("添加会议 ")
    public CommonResult addCmsMeeting(@RequestBody CmsMeetingAddDto dto) {
        return iCmsMeetingService.addCmsMeeting(dto);
    }

    @DeleteMapping("/deleteCmsMeetingById")
    @ApiOperation("通过会议 id批量删除会议 ")
    public CommonResult deleteCmsMeetingById(@RequestBody List<Long> list) {
        return  iCmsMeetingService.deleteCmsMeeting(list);
    }
    @PutMapping("/updateCmsMeetingById")
    @ApiOperation("通过会议id修改会议 ")
    public CommonResult updateCmsMeetingById(@RequestBody CmsMeetingUpdateDto updateDto) {
        return iCmsMeetingService.updateCmsMeeting(updateDto);
    }
}

