package com.jhc.BackstageController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsLeaveAddDto;
import com.jhc.DTO.CmsLeaveDto;
import com.jhc.DTO.CmsLeaveUpdateDto;
import com.jhc.VO.CmsLeaveVo;
import com.jhc.service.ICmsLeaveService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 18:50
 */
@RestController
@RequestMapping("/backstageCmsLeave")
@Api(tags = "请假表接口")
public class BackstageCmsLeaveController {
    @Autowired
    private ICmsLeaveService leaveService;

    @ApiOperation("后台获取请假列表 ")
    @GetMapping("/getCmsLeave")
    public CommonResult getLeaves(CmsLeaveDto pageVo){
        IPage<CmsLeaveVo> cmsLeavePageData = leaveService.getCmsLeavePageData(pageVo);
        return CommonResult.success(cmsLeavePageData);
    }

    @PostMapping("addCmsLeave")
    @ApiOperation("添加请假 ")
    public CommonResult addCmsLeave(@RequestBody CmsLeaveAddDto dto) {
        return leaveService.addCmsLeave(dto);
    }

    @DeleteMapping("/deleteCmsLeaveById")
    @ApiOperation("通过请假 id批量删除请假 ")
    public CommonResult deleteCmsLeaveById(@RequestBody List<Long> list) {
        return  leaveService.deleteCmsLeave(list);
    }
    @PutMapping("/updateCmsLeaveById")
    @ApiOperation("通过请假id修改请假 ")
    public CommonResult updateCmsLeaveById(@RequestBody CmsLeaveUpdateDto updateDto) {
        return leaveService.updateCmsLeave(updateDto);
    }

}
