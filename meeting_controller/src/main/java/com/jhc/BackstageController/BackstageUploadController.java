package com.jhc.BackstageController;


import com.jhc.service.ICmsImagesService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 专业档案  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-09-16
 */
@RestController
@RequestMapping("/backstageUpload")
@Api(tags="基础接口")
public class BackstageUploadController {

    @Autowired
    private ICmsImagesService imagesService;

    @PostMapping("/fileUpload")
    @ApiOperation("文件上传")
    public CommonResult fileUpload(@RequestParam(name = "file") MultipartFile file){
        String attachment = imagesService.fileUpload(file);
        if (attachment!=null){
            return CommonResult.success(attachment);
        }
        return CommonResult.failed("上传失败");
    }

}

