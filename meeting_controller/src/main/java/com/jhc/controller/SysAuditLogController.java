package com.jhc.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 访问日志信息  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-07-02
 */
@RestController
@RequestMapping("/sysAuditLog")
@Api(tags="系统 访问日志信息")
public class SysAuditLogController {

}

