package com.jhc.controller;


import com.jhc.BO.AdminSaveDetails;
import com.jhc.BO.AdminUserDetails;
import com.jhc.service.ICmsFaceService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-06-15
 */
@RestController
@RequestMapping("/cmsFace")
@Api(tags = "新人脸接口")
public class CmsFaceController {
    @Autowired
    private ICmsFaceService cmsFaceService;

    @Autowired
    private AdminSaveDetails saveDetails;

    @PostMapping("/createFace")
    @ApiOperation("创建人脸(二进制)")
    public CommonResult createFace(@RequestPart MultipartFile file){
        AdminUserDetails adminUserDetails = (AdminUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = adminUserDetails.getUserRealName();
        String userNumber =adminUserDetails.getUsername();
        return cmsFaceService.createFace(file,userNumber,userName);
    }

    @GetMapping("/getFaceInfo")
    @ApiOperation("获取人脸信息")
    public CommonResult getFaceInfo(String userNumber){
        return cmsFaceService.getFaceInfo(userNumber);
    }


    @PostMapping("/updateFace")
    @ApiOperation("更新人脸(二进制)")
    public CommonResult updateFace(@RequestPart MultipartFile file){
        AdminUserDetails adminUserDetails = (AdminUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = adminUserDetails.getUserRealName();
        String userNumber =adminUserDetails.getUsername();
        return cmsFaceService.updateFace(file,userNumber,userName);
    }

    @PostMapping("/searchFace")
    @ApiOperation("人脸搜索(二进制)")
    public CommonResult searchFace(MultipartFile file){
        return cmsFaceService.searchFace(file);
    }

    @PostMapping("/faceSignOnline")
    @ApiOperation("人脸签到(线上 二进制)")
    public CommonResult faceSignOnline(@RequestParam(value = "file",required = false) MultipartFile file,@RequestParam Long meetingId){
        return cmsFaceService.faceSignOnline(file,meetingId,saveDetails.getSysUser().getNumber());
    }

    @PostMapping("/faceSignOffline")
    @ApiOperation("人脸签到(线下 二进制)")
    public CommonResult faceSignOffline(@RequestParam(value = "file",required = false) MultipartFile file,@RequestParam Long meetingId,@RequestParam Double lng,@RequestParam Double lat){
        return cmsFaceService.faceSignOffline(file,meetingId,saveDetails.getSysUser().getNumber(),lng,lat);
    }

    @PostMapping("/checkFace")
    @ApiOperation("比对自己的人脸(二进制)")
    public CommonResult checkFace(MultipartFile file){
        return cmsFaceService.checkFace(file,saveDetails.getSysUser().getNumber());
    }
}

