package com.jhc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 签到表 前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
@RestController
@RequestMapping("/sysConfig")
public class SysConfigController {

}

