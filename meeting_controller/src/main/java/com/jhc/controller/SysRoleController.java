package com.jhc.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jhc.DO.BaseDo;
import com.jhc.DO.SysRole;
import com.jhc.DTO.SysAddUserRoleDto;
import com.jhc.DTO.SysRoleAddDto;
import com.jhc.DTO.SysRoleUpdateDto;
import com.jhc.service.ISysRoleService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * cms_role  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-07-02
 */
@RestController
@RequestMapping("/sysRole")
@Api(tags = "系统 权限管理接口")
public class SysRoleController {
    @Autowired
    private ISysRoleService roleService;

    @ApiOperation("新增角色")
    @PostMapping("/addRole")
    public CommonResult addRole(@RequestBody SysRoleAddDto sysRoleAddDto) {
        boolean result = roleService.addRole(sysRoleAddDto);
        if (result) {
            return CommonResult.success("新增角色成功");
        } else {
            return CommonResult.failed("新增失败");
        }
    }

    @ApiOperation("增加用户角色")
    @PutMapping("/addUserRole")
    public CommonResult addUserRole(@RequestBody SysAddUserRoleDto addUserRoleDto) {
        boolean b = roleService.addUserRole(addUserRoleDto);
        if (b) {
            return CommonResult.success("给用户添加角色成功");
        } else {
            return CommonResult.failed("给用户添加角色失败");
        }
    }

    @ApiOperation("删除角色(可批量)")
    @DeleteMapping()
    public CommonResult deletesRole( @RequestBody List<Long> ids) {
        boolean result = roleService.removeByIds(ids);
        if (result) {
            return CommonResult.success("删除角色成功");
        } else {
            return CommonResult.failed("删除角色失败");
        }
    }

    @ApiOperation("更新角色")
    @PutMapping("/UpdateRole")
    public CommonResult updateRole(@RequestBody SysRoleUpdateDto sysRoleUpdateDto) {
        boolean result = roleService.updateRole(sysRoleUpdateDto);
        if (result) {
            return CommonResult.success("更新角色成功");
        } else {
            return CommonResult.failed("更新角色失败");
        }
    }

    @ApiOperation("查询所有角色")
    @GetMapping("/all")
    public CommonResult searchRole() {
        return CommonResult.success(roleService.list(new QueryWrapper<SysRole>().lambda().orderByAsc(BaseDo::getCreateTime)));
    }

}

