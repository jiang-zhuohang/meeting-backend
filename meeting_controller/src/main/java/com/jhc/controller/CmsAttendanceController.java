package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsAttendanceAddDto;
import com.jhc.DTO.CmsAttendanceDto;
import com.jhc.DTO.CmsAttendanceUpdateDto;
import com.jhc.VO.CmsAttendanceVo;
import com.jhc.service.ICmsAttendanceService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
@RestController
@RequestMapping("/cmsAttendance")
@Api(tags="考勤接口")
public class CmsAttendanceController {

    @Autowired
    private ICmsAttendanceService iCmsAttendanceService;

    @ApiOperation("前台获取考勤列表 ")
    @GetMapping("/getCmsNotice")
    public CommonResult getAttendances(CmsAttendanceDto pageVo){
        IPage<CmsAttendanceVo> cmsAttendancePageData = iCmsAttendanceService.getCmsAttendancePageData(pageVo);
        return CommonResult.success(cmsAttendancePageData);
    }

    @PostMapping("addCmsAttendance")
    @ApiOperation("添加考勤 ")
    public CommonResult addCmsAttendance(@RequestBody CmsAttendanceAddDto dto) {
        return iCmsAttendanceService.addCmsAttendance(dto);
    }

    @DeleteMapping("/deleteCmsAttendanceById")
    @ApiOperation("通过考勤 id批量删除考勤 ")
    public CommonResult deleteCmsAttendanceById(@RequestBody List<Long> list) {
        return  iCmsAttendanceService.deleteCmsAttendance(list);
    }
    @PutMapping("/updateCmsAttendanceById")
    @ApiOperation("通过考勤id修改考勤 ")
    public CommonResult updateCmsAttendanceById(@RequestBody CmsAttendanceUpdateDto updateDto) {
        return iCmsAttendanceService.updateCmsAttendance(updateDto);
    }

}

