package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DO.Message;
import com.jhc.DTO.*;
import com.jhc.VO.MessagePageVo;
import com.jhc.VO.MessageVO;
import com.jhc.service.IMessageService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 消息表 前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-07
 */
@RestController
@RequestMapping("/message")
@Api(tags = "消息接口")
public class MessageController {

    @Autowired
    private IMessageService imessageService;

    @GetMapping("/push")
    @ApiOperation("同步redis缓存")
    public CommonResult push(){
        return imessageService.push();
    }

    @PostMapping("addMessage")
    @ApiOperation("添加消息")
    public CommonResult addMessage(@RequestBody MessageAddDto messageAddDto) {
        return imessageService.addMessage(messageAddDto);

    }
    @PostMapping("addFormatMessage")
    @ApiOperation("添加格式化消息")
    public CommonResult addFormatMessage(@RequestBody MessageFormatAddDto dto) {
        return imessageService.addFormatMessage(dto);

    }

    @DeleteMapping("/deleteMessageById")
    @ApiOperation("通过消息id批量删除消息")
    public CommonResult deleteMessageById(@RequestBody List<Long> list) {
        boolean b = imessageService.deleteMessageById(list);
        if (b) {
            return CommonResult.success("批量删除成功");
        } else {
            return CommonResult.failed("批量删除失败");
        }
    }

    @PutMapping("updateMessageById")
    @ApiOperation("通过消息id修改消息")
    public CommonResult updateMessageById(@RequestBody MessageUpdateDto messageUpdateDto) {
        boolean b = imessageService.updateMessageById(messageUpdateDto);
        if (b) {
            return CommonResult.success("修改成功");
        } else {
            return CommonResult.failed("修改失败,没有该条数据");
        }
    }

    @ApiOperation("获取消息,分页条件查询")
    @GetMapping("/getMessagePageData")
    public CommonResult getMessagePageData(MessageDto pageVo) {
        IPage<MessageVO> messageVoIPage = imessageService.getMessagePageData(pageVo);
        return CommonResult.success(messageVoIPage);
    }

    @ApiOperation("获取用户消息，分页")
    @GetMapping("/getUserMessagePageData")
    public CommonResult getUserMessagePageData(MessagePageDto dto) {
        IPage<MessagePageVo> page = imessageService.getUserMessageData (dto);
        return CommonResult.success(page);
    }

    @ApiOperation ("批量修改消息状态")
    @PutMapping("/updateIsRead")
    public CommonResult updateIsRead(@RequestBody List<MessageUpdateIsReadDto> dtoList){
        return imessageService.updateIsRead (dtoList);
    }
}

