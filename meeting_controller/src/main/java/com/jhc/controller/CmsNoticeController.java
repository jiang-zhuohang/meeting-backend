package com.jhc.controller;


import com.jhc.DTO.CmsNoticeAddDto;
import com.jhc.DTO.CmsNoticeDto;
import com.jhc.DTO.CmsNoticeUpdateDto;
import com.jhc.service.ICmsNoticeService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 公告 前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-01-08
 */
@RestController
@RequestMapping("/cmsNotice")
@Api(tags="数据维护接口")
public class CmsNoticeController {
    @Autowired
    private ICmsNoticeService iCmsNoticeService;

    @ApiOperation("前台获取公告 ")
    @GetMapping("/getCmsNotice")
    public CommonResult getCmsNotice() {
        return iCmsNoticeService.getCmsNoticeA();
    }

    @ApiOperation("根据id获取公告详情")
    @GetMapping("/getCmsNoticeById")
    public CommonResult getCmsNoticeById(Long id) {
        return iCmsNoticeService.getCmsNoticeById(id);
    }

}

