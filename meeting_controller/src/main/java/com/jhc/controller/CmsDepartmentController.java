package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsDepartmentAddDto;
import com.jhc.DTO.CmsDepartmentDto;
import com.jhc.DTO.CmsDepartmentUpdateDto;
import com.jhc.VO.CmsDepartmentVo;
import com.jhc.service.ICmsDepartmentService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
@RestController
@RequestMapping("/cmsDepartment")
@Api(tags="部门接口")
public class CmsDepartmentController {
    @Autowired
    private ICmsDepartmentService iCmsDepartmentService;

    @ApiOperation("前台获取部门列表 ")
    @GetMapping("/getCmsDepartment")
    public CommonResult getDepartments(CmsDepartmentDto pageVo){
        IPage<CmsDepartmentVo> cmsDepartmentPageData = iCmsDepartmentService.getCmsDepartmentPageData(pageVo);
        return CommonResult.success(cmsDepartmentPageData);
    }

    @PostMapping("addCmsDepartment")
    @ApiOperation("添加部门 ")
    public CommonResult addCmsDepartment(@RequestBody CmsDepartmentAddDto dto) {
        return iCmsDepartmentService.addCmsDepartment(dto);
    }

    @DeleteMapping("/deleteCmsDepartmentById")
    @ApiOperation("通过部门 id批量删除部门 ")
    public CommonResult deleteCmsDepartmentById(@RequestBody List<Long> list) {
        return  iCmsDepartmentService.deleteCmsDepartment(list);
    }
    @PutMapping("/updateCmsDepartmentById")
    @ApiOperation("通过部门id修改部门 ")
    public CommonResult updateCmsDepartmentById(@RequestBody CmsDepartmentUpdateDto updateDto) {
        return iCmsDepartmentService.updateCmsDepartment(updateDto);
    }

    @GetMapping("/departmentList")
    @ApiOperation("获取部门下拉框")
    public CommonResult  getDepartmentList(){
        return CommonResult.success(iCmsDepartmentService.getDepartmentList());
    }

    @GetMapping("/getDepartmentUserList")
    @ApiOperation ("前台获取通过部门分类的用户列表")
    public CommonResult getDepartmentUserList(){
        return CommonResult.success(iCmsDepartmentService.getDepartmentUserList());
    }

}

