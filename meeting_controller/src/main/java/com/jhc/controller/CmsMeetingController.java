package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsMeetingAddDto;
import com.jhc.DTO.CmsMeetingDto;
import com.jhc.DTO.CmsMeetingUpdateDto;
import com.jhc.DTO.ManualSignDto;
import com.jhc.VO.CmsMeetingVo;
import com.jhc.VO.MeetingInfoVo;
import com.jhc.service.ICmsMeetingService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@RestController
@RequestMapping("/cmsMeeting")
@Api(tags="会议接口")
public class CmsMeetingController {

    @Autowired
    private ICmsMeetingService iCmsMeetingService;

    @GetMapping("/push")
    @ApiOperation("更新redis")
    public CommonResult push(){
        return iCmsMeetingService.push();
    }

    @GetMapping("/ifApproval")
    @ApiOperation("是否有会议需要审批")
    public CommonResult ifApproval(){
        return CommonResult.success(iCmsMeetingService.ifApproval());
    }

    @ApiOperation("前台获取会议列表 ")
    @GetMapping("/getCmsMeeting")
    public CommonResult getMeetings(String status){
        return iCmsMeetingService.getCmsMeetingList(status);
    }

    @ApiOperation("获取用户开会会议列表 ")
    @GetMapping("/getUserInitiatedMeetingList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userNumber",value = "用户编号",required = true),
            @ApiImplicitParam(name = "status",value = "0全部 1非历史会议  2历史会议",required = true)
    })
    public CommonResult getUserInitiatedMeetingList(String userNumber, String status){
        return iCmsMeetingService.getUserInitiatedMeetingList(userNumber,status);
    }

    @ApiOperation("获取用户参会会议列表 ")
    @GetMapping("/getUserJoinMeetingList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userNumber",value = "用户编号",required = true),
            @ApiImplicitParam(name = "status",value = "0全部 1非历史会议  2历史会议",required = true)
    })
    public CommonResult getUserJoinMeetingList(String userNumber, String status){
        return iCmsMeetingService.getUserJoinMeetingList(userNumber,status);
    }

    @ApiOperation("根据会议id获取会议详情 ")
    @GetMapping("/getCmsMeetingById")
    public CommonResult getCmsMeetingById(Long id){
        MeetingInfoVo cmsMeetingById = iCmsMeetingService.getCmsMeetingById(id);
        return CommonResult.success(cmsMeetingById);
    }

    @PostMapping("addCmsMeeting")
    @ApiOperation("添加会议 ")
    public CommonResult addCmsMeeting(@RequestBody CmsMeetingAddDto dto) {
        return iCmsMeetingService.addCmsMeeting(dto);
    }

    @DeleteMapping("/deleteCmsMeetingById")
    @ApiOperation("通过会议 id批量删除会议 ")
    public CommonResult deleteCmsMeetingById(@RequestBody List<Long> list) {
        return  iCmsMeetingService.deleteCmsMeeting(list);
    }
    @PutMapping("/updateCmsMeetingById")
    @ApiOperation("通过会议id修改会议 ")
    public CommonResult updateCmsMeetingById(@RequestBody CmsMeetingUpdateDto updateDto) {
        return iCmsMeetingService.updateCmsMeeting(updateDto);
    }

    @PostMapping("/manualSign")
    @ApiOperation("给会议成员手动签到")
    public CommonResult manualSign(@RequestBody ManualSignDto manualSignDto){
        return iCmsMeetingService.manualSign(manualSignDto);
    }

    @PostMapping("/endMeeting")
    @ApiOperation("开会人手动结束会议")
    public CommonResult endMeeting(@RequestParam Long meetingId){
        return iCmsMeetingService.endMeeting(meetingId);
    }
}

