package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsRoomAddDto;
import com.jhc.DTO.CmsRoomDto;
import com.jhc.DTO.CmsRoomUpdateDto;
import com.jhc.VO.CmsRoomVo;
import com.jhc.service.ICmsRoomService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
@RestController
@RequestMapping("/cmsRoom")
@Api(tags = "会议室接口")
public class CmsRoomController {
    @Autowired
    private ICmsRoomService roomService;

    @ApiOperation("前台根据状态获取会议室下拉框 ")
    @GetMapping("/getCmsRoomByStatus")
    @ApiImplicitParam(name="status",value = "0禁用的 1启用的")
    public CommonResult getRoomsByStatus( Long status){
        return CommonResult.success(roomService.getRoomsByStatus(status));
    }

    @PostMapping("addCmsRoom")
    @ApiOperation("添加会议室 ")
    public CommonResult addCmsRoom(@RequestBody CmsRoomAddDto dto) {
        return roomService.addCmsRoom(dto);
    }

    @DeleteMapping("/deleteCmsRoomById")
    @ApiOperation("通过会议室 id批量删除会议室 ")
    public CommonResult deleteCmsRoomById(@RequestBody List<Long> list) {
        return  roomService.deleteCmsRoom(list);
    }
    @PutMapping("/updateCmsRoomById")
    @ApiOperation("通过会议室id修改会议室 ")
    public CommonResult updateCmsRoomById(@RequestBody CmsRoomUpdateDto updateDto) {
        return roomService.updateCmsRoom(updateDto);
    }
}

