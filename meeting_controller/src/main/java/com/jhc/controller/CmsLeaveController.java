package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.DTO.CmsLeaveAddDto;
import com.jhc.DTO.CmsLeaveDto;
import com.jhc.DTO.CmsLeaveUpdateDto;
import com.jhc.VO.CmsLeaveVo;
import com.jhc.service.ICmsLeaveService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
@RestController
@RequestMapping("/cmsLeave")
@Api(tags = "请假表接口")
public class CmsLeaveController {
    @Autowired
    private ICmsLeaveService leaveService;

    @ApiOperation("前台获取请假列表 ")
    @GetMapping("/getCmsLeave")
    public CommonResult getLeaves(CmsLeaveDto pageVo){
        IPage<CmsLeaveVo> cmsLeavePageData = leaveService.getCmsLeavePageData(pageVo);
        return CommonResult.success(cmsLeavePageData);
    }

    @PostMapping("addCmsLeave")
    @ApiOperation("添加请假 ")
    public CommonResult addCmsLeave(@RequestBody CmsLeaveAddDto dto) {
        return leaveService.addCmsLeave(dto);
    }

    @DeleteMapping("/deleteCmsLeaveById")
    @ApiOperation("通过请假 id批量删除请假 ")
    public CommonResult deleteCmsLeaveById(@RequestBody List<Long> list) {
        return  leaveService.deleteCmsLeave(list);
    }

    @PutMapping("/updateCmsLeaveById")
    @ApiOperation("通过请假id修改请假 ")
    public CommonResult updateCmsLeaveById(@RequestBody CmsLeaveUpdateDto updateDto) {
        return leaveService.updateCmsLeave(updateDto);
    }

    @GetMapping("/getMeetingUserLeave")
    @ApiOperation("获取会议用户请假历史")
    public CommonResult getMeetingUserLeave(String meetingId,String userNumber){
        return leaveService.getMeetingUserLeave(meetingId,userNumber);
    }
}

