package com.jhc.config;


import com.alibaba.fastjson.JSON;
import com.jhc.VO.RedisUserInfo;
import com.jhc.service.RedisService;
import com.jhc.utils.CookieUtils;
import com.jhc.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.manager.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Slf4j
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    @Value("${jwt.expiration}")
    private Long expiration;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtUtils jwtTokenUtil;
    @Autowired
    private RedisService redisService;
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    private static final String CACHE_PUNCH_REGION = "USER";

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        // 拿到请求头
        String authHeader = httpServletRequest.getHeader(this.tokenHeader);

        if (authHeader != null && authHeader.startsWith(this.tokenHead)) {
            String authToken = authHeader.substring(this.tokenHead.length());
            // 解密username
            String username = jwtTokenUtil.getUserNameFromToken(authToken);
            // 从redis里面拿值
            String redisValue = redisService.get(CACHE_PUNCH_REGION, username);

            if (redisValue != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                try {
                    UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    //token快过期时更新(一天)
                    Date expiredDateFromToken = jwtTokenUtil.getExpiredDateFromToken(authToken);
                    Date now = new Date();
                    long between = expiredDateFromToken.getTime() - now.getTime();
                    if(between < (24*3600000)){ //24*3600000
                        String token = jwtTokenUtil.generateToken(userDetails);
                        System.out.println(token);
                        RedisUserInfo redisUserInfo = JSON.parseObject(redisService.get("USER",username), RedisUserInfo.class);
                        redisUserInfo.setToken(token);
                        redisService.set("USER", username,JSON.toJSONString(redisUserInfo),expiration);
                        httpServletResponse.addHeader("token",tokenHead+token);
                        httpServletResponse.setHeader("Access-Control-Expose-Headers", "token");
                    }
                     } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
            if (redisValue == null) {

                String requestURI = httpServletRequest.getRequestURI();
                if (requestURI.equals("/CollectKey/getIsCollectKeyByNumber")||requestURI.equals("/Point/getPoint")) {
                    throw new RuntimeException("401");
                }
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
