package com.jhc.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration("visualServerConfig")
@EnableTransactionManagement
public class ServerConfig {

    @Configuration
    @MapperScan("com.visual.face.search.server.mapper")
    public static class MapperConfig {}

    @Configuration
    @ComponentScan("com.jhc.config")
    public static class SearchConfig {}

    @Configuration
    @ComponentScan({"com.jhc.service"})
    public static class ServiceConfig {}

    @Configuration
    @ComponentScan({"com.jhc.face.controller"})
    public static class ControllerConfig {}

}
