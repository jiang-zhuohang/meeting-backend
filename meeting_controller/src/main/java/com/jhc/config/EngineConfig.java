package com.jhc.config;

import com.alibaba.proxima.be.client.ConnectParam;
import com.alibaba.proxima.be.client.ProximaGrpcSearchClient;
import com.alibaba.proxima.be.client.ProximaSearchClient;

import com.jhc.engine.api.SearchEngine;
import com.jhc.engine.impl.ProximaSearchEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("visualEngineConfig")
public class EngineConfig {
    //日志
    public Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${face.engine.proxima.host}")
    private String proximaHost;
    @Value("${face.engine.proxima.port:16000}")
    private Integer proximaPort;


    @Bean(name = "visualSearchEngine")
    public SearchEngine getSearchEngine(){

            logger.info("current vector engine is proxima");
            ConnectParam connectParam = ConnectParam.newBuilder().withHost(proximaHost).withPort(proximaPort).build();
            ProximaSearchClient client = new ProximaGrpcSearchClient(connectParam);
            return new ProximaSearchEngine(client);
        }
}
