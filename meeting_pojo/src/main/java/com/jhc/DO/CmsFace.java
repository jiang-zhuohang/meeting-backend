package com.jhc.DO;

import com.jhc.DO.BaseDo;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-06-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CmsFace extends BaseDo {

    private static final long serialVersionUID=1L;

    /**
     * 用户编号
     */
    private String userNumber;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 人脸图片
     */
    private String facePhoto;

    /**
     * 人脸分数
     */
    private Float faceScore;

    /**
     * 人脸向量
     */
    private String faceVector;


}
