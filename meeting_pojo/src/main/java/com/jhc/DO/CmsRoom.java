package com.jhc.DO;

import com.jhc.DO.BaseDo;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CmsRoom extends BaseDo {

    private static final long serialVersionUID=1L;

    /**
     * 会议室编号
     */
    private String number;

    /**
     * 会议室名称
     */
    private String name;

    /**
     * 容纳人数
     */
    private Integer size;

    /**
     * 状态 0不可预约 1可预约
     */
    private Integer status;


}
