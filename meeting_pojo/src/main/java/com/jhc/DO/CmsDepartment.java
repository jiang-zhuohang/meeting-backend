package com.jhc.DO;

import com.baomidou.mybatisplus.annotation.TableField;
import com.jhc.DO.BaseDo;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CmsDepartment extends BaseDo {

    private static final long serialVersionUID=1L;

    /**
     * 部门名称
     */
    @TableField(value="`name`")
    private String name;

    /**
     * 部门编号
     */
    private String number;

    /**
     * 负责人编号
     */
    private String userNumber;

    /**
     * 负责人姓名
     */
    private String userName;


}
