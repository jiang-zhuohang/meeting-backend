package com.jhc.DO;

import com.jhc.DO.BaseDo;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CmsLeader extends BaseDo {

    private static final long serialVersionUID=1L;

    /**
     * 启停状态 1->启用 0->禁用
     */
    private Boolean isEnable;


    /**
     * 编号
     */
    private String number;

    /**
     * 姓名
     */
    private String name;

    /**
     * 部门编号
     */
    private String departmentNumber;

    /**
     * 性别 0->默认，1->男，2->女，3->保密
     */
    private Integer sex;

    /**
     * 电话
     */
    private String phone;


}
