package com.jhc.DO;

import com.jhc.DO.BaseDo;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CmsAttendance extends BaseDo {

    private static final long serialVersionUID=1L;

    /**
     * 用户编号
     */
    private String userNumber;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 会议id
     */
    private Long meetingId;

    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 是否请假 0否 1请假
     */
    private Integer isLeave;

    /**
     * 签到状态 0未签到 1已签到 2迟到
     */
    private Integer signInStatus;

    /**
     * 签到方式 1人脸  2手动
     */
    private Integer signInMethod;

    /**
     * 签到时间
     */
    private LocalDateTime signInTime;

    /**
     * 签退状态 0未签退 1已签退 3早退
     */
    private Integer signOutStatus;

    /**
     *  签退方式 1人脸  2手动
     */
    private Integer signOutMethod;

    /**
     * 签退时间
     */
    private LocalDateTime signOutTime;


}
