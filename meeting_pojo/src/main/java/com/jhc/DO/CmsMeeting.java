package com.jhc.DO;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jhc.DO.BaseDo;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CmsMeeting extends BaseDo {

    private static final long serialVersionUID=1L;

    /**
     * 会议名称
     */
    private String name;

    /**
     * 会议描述
     */
    @TableField("`describe`")
    private String describe;

    /**
     * 发起人编号
     */
    private String userNumber;

    /**
     * 发起人姓名
     */
    private String userName;

    /**
     * 会议成员
     */
    private String memberNumber;

    /**
     * 会议类型 0线上 1线下
     */
    private Integer type;

    /**
     * 会议室编号
     */
    private String roomNumber;

    /**
     * 开始时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime endTime;

    /**
     * 签到时间范围
     */
    private Integer timeFrame;

    /**
     * 会议状态 0审核中 1已通过 2未通过 3签到中 4进行中 5签退中 6已结束
     */
    private Integer meetingStatus;


}
