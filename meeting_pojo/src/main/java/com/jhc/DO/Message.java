package com.jhc.DO;

import com.jhc.DO.BaseDo;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Message extends BaseDo {

    private static final long serialVersionUID=1L;

    /**
     * 发送端编号
     */
    private String sendNumber;

    /**
     * 发送者姓名
     */
    private String sendName;

    /**
     * 接收端编号
     */
    private String acceptNumber;

    /**
     * 内容 
     */
    private String content;

    /**
     * 类型  （用户私信   系统通知   被评论回复等消息通知）
     */
    private String type;

    /**
     * 状态 0--未读  1-- 已读
     */
    private Boolean isRead;


}
