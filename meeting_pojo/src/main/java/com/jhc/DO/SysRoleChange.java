package com.jhc.DO;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: jiangzhuohang
 * @Date: 2021/09/13/23:45
 * @Description:
 */
@Data
public class SysRoleChange {

  private  List<String>  authList;

}
