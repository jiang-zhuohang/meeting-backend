package com.jhc.DO;

import com.jhc.DO.BaseDo;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CmsLeave extends BaseDo {

    private static final long serialVersionUID=1L;

    /**
     * 请假人姓名
     */
    private String userName;

    /**
     * 请假人编号
     */
    private String userNumber;

    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 会议id
     */
    private Long meetingId;

    /**
     * 请假理由
     */
    private String reason;

    /**
     * 请假状态0审核中 1通过 2拒绝
     */
    private Integer status;


}
