package com.jhc.BO;


import com.jhc.DO.SysUser;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: jiangzhuohang
 * @Date: 2021/07/01/16:23
 * @Description:
 */
@Data
public class AdminSaveDetails {

    private SysUser sysUser;
}
