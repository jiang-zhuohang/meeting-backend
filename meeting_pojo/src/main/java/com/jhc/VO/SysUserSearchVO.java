package com.jhc.VO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 */
@Data
public class SysUserSearchVO {
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 用户编号
     */
    private String number;

    /**
     * 姓名
     */
    private String name;


//    /**
//     * 权限集合
//     */
//    private String authData;


}
