package com.jhc.VO;

import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/15 16:25
 */
@Data
public class UserBaseInfoVo {
    /**
     * 用户编号
     */
    private String number;

    /**
     * 姓名
     */
    private String name;
}
