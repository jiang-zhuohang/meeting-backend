package com.jhc.VO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;


/**
 * @author jiangzhuohang
 * @create 2022/3/18 14:52
 */
@Data
public class CmsLeaderVo {
    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id ;


    /**
     * 编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private String number;

    /**
     * 姓名
     */
    private String name;

    /**
     * 部门编号
     */
    private String departmentNumber;

    /**
     * 性别 0->默认，1->男，2->女，3->保密
     */
    private Integer sex;

    /**
     * 电话
     */
    private String phone;

}
