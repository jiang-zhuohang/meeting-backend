package com.jhc.VO;

import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/15 14:49
 */
@Data
public class RoomListVo {
    /**
     * 会议室编号
     */
    private String number;

    /**
     * 会议室名称
     */
    private String name;

}
