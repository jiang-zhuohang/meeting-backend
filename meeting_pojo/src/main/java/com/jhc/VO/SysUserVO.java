package com.jhc.VO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 *
 */
@Data
public class SysUserVO implements Serializable {
    private static final long serialVersionUID=1L;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 用户编号
     */
    private String number;

    /**
     * 姓名
     */
    private String name;

    /**
     * 类型 10普通员工 20管理层
     */
    private Integer type;

    /**
     * 部门编号
     */
    private String departmentNumber;

    /**
     * 电话
     */
    private String phone;
    /**
     * 权限集合
     */
    private String authData;

    /**
     * 性别 0默认1男2女3保密
     * */
    private Integer sex;

}
