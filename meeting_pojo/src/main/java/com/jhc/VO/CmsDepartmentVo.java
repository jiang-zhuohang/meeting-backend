package com.jhc.VO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/5 22:48
 */
@Data
public class CmsDepartmentVo {

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id ;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 部门编号
     */
    private String number;


    /**
     * 负责人编号
     */
    private String userNumber;

    /**
     * 负责人姓名
     */
    private String userName;
}
