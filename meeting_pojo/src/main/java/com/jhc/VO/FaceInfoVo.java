package com.jhc.VO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/6/15 18:41
 */
@Data
public class FaceInfoVo {

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id ;

    /**
     * 用户编号
     */
    private String userNumber;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 人脸图片
     */
    private String facePhoto;

    /**
     * 人脸分数
     */
    private Float faceScore;

}
