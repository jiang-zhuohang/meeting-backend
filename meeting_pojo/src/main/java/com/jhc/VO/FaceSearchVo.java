package com.jhc.VO;

import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/6/16 9:09
 */
@Data
public class FaceSearchVo {
    /**
     * 用户编号
     */
    private String userNumber;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 人脸图片
     */
    private String facePhoto;

    /**
     * 置信度
     */
    private Float confidence;
}
