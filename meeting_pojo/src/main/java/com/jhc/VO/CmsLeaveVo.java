package com.jhc.VO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 11:00
 */
@Data
public class CmsLeaveVo {
    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id ;

    /**
     * 请假人姓名
     */
    private String userName;

    /**
     * 请假人编号
     */
    private String userNumber;

    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 会议id
     */
    private Long meetingId;

    /**
     * 请假理由
     */
    private String reason;

    /**
     * 请假状态
     */
    private Integer status;
}
