package com.jhc.VO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 10:06
 */
@Data
public class CmsRoomVo {

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id ;

    /**
     * 会议室编号
     */
    private String number;

    /**
     * 会议室名称
     */
    private String name;

    /**
     * 容纳人数
     */
    private Integer size;

    /**
     * 状态 0不可预约 1可预约
     */
    private Integer status;
}
