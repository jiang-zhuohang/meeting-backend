package com.jhc.VO;

import lombok.Data;

@Data
public class SysUserTopVo {

    /**
     * 性别
     */
    private Integer sex;

    /**
     *学号或者职工号
     */
    private String number;

    /**
     * 姓名
     */
    private String name;

   /**
     * 学生/老师 班级
     */
    private String commonClass;

    /**
     * 学院
     */
    private String College;


}
