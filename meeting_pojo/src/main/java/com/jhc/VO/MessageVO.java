package com.jhc.VO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class MessageVO {

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 发送端编号
     */
    private String sendNumber;

    /**
     * 发送者姓名
     */
    private String sendName;

    /**
     * 接收端编号
     */
    private String acceptNumber;

    /**
     * 内容
     */
    private String content;

    /**
     * 类型  （用户私信   系统通知   被评论回复等消息通知）
     */
    private String type;

    /**
     * 状态 0--未读  1-- 已读
     */
    private Boolean isRead;

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
}
