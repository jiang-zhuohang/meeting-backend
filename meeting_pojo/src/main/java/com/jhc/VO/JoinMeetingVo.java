package com.jhc.VO;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 历史参会记录VO
 * @author jiangzhuohang
 * @create 2022/5/11 8:46
 */
@Data
public class JoinMeetingVo {
    /**
     * 会议id
     */
    private Long Id;

    /**
     * 会议名称
     */
    private String name;

    /**
     * 发起人姓名
     */
    private String userName;

    /**
     * 会议类型 0线上 1线下
     */
    private Integer type;

    /**
     * 会议室编号
     */
    private String roomNumber;

    /**
     * 是否请假 0否 1请假
     */
    private Boolean isLeave;

    /**
     * 签到状态 0未签到 1已签到 2迟到
     */
    private Integer signInStatus;

    /**
     * 签退状态 0未签退 1已签退 3早退
     */
    private Integer signOutStatus;


    /**
     * 开始时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime endTime;

}
