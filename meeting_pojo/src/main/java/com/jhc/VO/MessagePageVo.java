package com.jhc.VO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class MessagePageVo {
    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 信息内容
     */
    private String content;

    /**
     * 消息状态 0 -- 未读 ，1-- 已读
     */
    private Boolean isRead;

    /**
     * 时间
     */
    @ApiModelProperty("日期")
    @JsonFormat(pattern="yyyy年MM月dd日 HH:mm",timezone = "GMT+8")
    private Date updateTime;
}
