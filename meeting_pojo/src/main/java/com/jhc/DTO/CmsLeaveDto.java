package com.jhc.DTO;

import com.jhc.DO.BasePage;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 10:59
 */
@Data
public class CmsLeaveDto extends BasePage {

    /**
     * 请假人姓名
     */
    private String userName;

    /**
     * 请假人编号
     */
    private String userNumber;

    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 会议id
     */
    private Long meetingId;

    /**
     * 请假理由
     */
    private String reason;

    /**
     * 请假状态 0审核中 1已通过 2未通过
     */
    private Integer status;
}
