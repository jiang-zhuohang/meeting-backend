package com.jhc.DTO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author jiangzhuohang
 * @create 2022/3/18 14:57
 */
@Data
public class CmsStaffAddDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty("编号")
    private String number;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 部门编号
     */
    @ApiModelProperty("部门编号")
    private String departmentNumber;

    /**
     * 性别 0->默认，1->男，2->女，3->保密
     */
    @ApiModelProperty("性别")
    private Integer sex;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phone;
}
