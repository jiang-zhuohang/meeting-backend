package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class MessageUpdateDto {

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 发送端编号
     */
    private String sendNumber;

    /**
     * 发送者姓名
     */
    private String sendName;

    /**
     * 接收端编号
     */
    private String acceptNumber;

    /**
     * 内容
     */
    private String content;

    /**
     * 类型  （用户私信   系统通知   被评论回复等消息通知）
     */
    private String type;
}
