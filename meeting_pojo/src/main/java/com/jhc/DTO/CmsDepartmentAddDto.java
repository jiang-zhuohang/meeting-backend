package com.jhc.DTO;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiangzhuohang
 * @create 2022/4/5 23:26
 */
@Data
public class CmsDepartmentAddDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 部门编号
     */
    private String number;


    /**
     * 负责人编号
     */
    private String userNumber;

    /**
     * 负责人姓名
     */
    private String userName;
}
