package com.jhc.DTO;

import com.jhc.DO.BasePage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MessagePageDto extends BasePage {
    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    public String userNumber;


}
