package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/29 16:09
 */
@Data
public class SysUserUpdateDto {
    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 用户编号
     */
    private String number;

    /**
     * 姓名
     */
    private String name;

    /**
     * 身份类型
     */
    private Integer type;

    /**
     * 部门编号
     */
    private String departmentNumber;

    /**
     * 0默认1男2女3保密
     */
    private Integer sex;

    /**
     * 电话
     */
    private String phone;

}
