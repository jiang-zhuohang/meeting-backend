package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.DO.BasePage;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: jiangzhuohang
 * @Date: 2022/4/6 18:56
 * @Description:
 */
@Data
public class SysConfigDto extends BasePage {

    /**
     * 名称
     */
    private String name;

    /**
     * 签到半径（米）
     */
    private Double radius;


    /**
     * 签到经度
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private BigDecimal lng;


    /**
     * 签到纬度
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private BigDecimal lat;



}
