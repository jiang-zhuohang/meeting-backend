package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/6/3 16:48
 */
@Data
public class ManualSignDto {
    /**
     * 会议id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("会议id")
    private Long meetingId;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userNumber;
}
