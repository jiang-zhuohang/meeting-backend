package com.jhc.DTO;

import com.jhc.DO.BasePage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/3/18 14:55
 */
@Data
public class CmsStaffDto extends BasePage {
    /**
     * 编号
     */
    @ApiModelProperty("编号")
    private String number;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 部门编号
     */
    @ApiModelProperty("部门编号")
    private String departmentNumber;

    /**
     * 性别 0->默认，1->男，2->女，3->保密
     */
    @ApiModelProperty("性别")
    private Integer sex;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phone;
}
