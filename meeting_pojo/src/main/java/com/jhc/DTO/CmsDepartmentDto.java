package com.jhc.DTO;

import com.jhc.DO.BasePage;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/4 13:14
 */
@Data
public class CmsDepartmentDto extends BasePage {


    /**
     * 部门名称
     */
    private String name;

    /**
     * 部门编号
     */
    private String number;


    /**
     * 负责人编号
     */
    private String userNumber;

    /**
     * 负责人姓名
     */
    private String userName;
}
