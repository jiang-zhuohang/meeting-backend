package com.jhc.DTO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: jiangzhuohang
 * @Date: 2022/4/6 18:56
 * @Description:
 */
@Data
public class SysConfigAddDto implements Serializable {

    /**
     * 名称
     */
    private String name;

    /**
     * 签到半径（米）
     */
    @ApiModelProperty(value = "签到半径（米）")
    private Double radius;


    /**
     * 签到经度
     */
    @ApiModelProperty(value = "签到经度")
    private BigDecimal lng;


    /**
     * 签到纬度
     */
    @ApiModelProperty(value = "签到纬度")
    private BigDecimal lat;


}
