package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: jiangzhuohang
 * @Date: 2022/4/6 18:56
 * @Description:
 */
@Data
public class SysConfigUpdateDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 签到配置id
     */
    @ApiModelProperty("签到配置id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 名称
     */
    private String name;

    /**
     * 签到半径（米）
     */
    @ApiModelProperty("签到半径（米）")
    private Double radius;


    /**
     * 签到经度
     */
    @ApiModelProperty("签到经度")
    @JsonSerialize(using = ToStringSerializer.class)
    private BigDecimal lng;


    /**
     * 签到纬度
     */
    @ApiModelProperty("签到纬度")
    @JsonSerialize(using = ToStringSerializer.class)
    private BigDecimal lat;



}
