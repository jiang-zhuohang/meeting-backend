package com.jhc.DTO;

import com.jhc.DO.BasePage;
import lombok.Data;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 9:57
 */
@Data
public class CmsRoomDto extends BasePage {
    /**
     * 会议室编号
     */
    private String number;

    /**
     * 会议室名称
     */
    private String name;

    /**
     * 容纳人数
     */
    private Integer size;

    /**
     * 状态 0不可预约 1可预约
     */
    private Integer status;
}
