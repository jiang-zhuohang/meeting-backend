package com.jhc.DTO;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 10:13
 */
@Data
public class CmsRoomAddDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 会议室编号
     */
    private String number;

    /**
     * 会议室名称
     */
    private String name;

    /**
     * 容纳人数
     */
    private Integer size;

    /**
     * 状态 0不可预约 1可预约
     */
    private Integer status;

}
