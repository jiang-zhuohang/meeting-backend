package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class MessageFormatAddDto {
    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 发送端
     */
    private String sendNumber;

    /**
     * 发送者姓名
     */
    private String sendName;

    /**
     * 接收端
     */
    private String acceptNumber;



    /**
     * 标题
     */
    private String title;


    /**
     * 系统消息，用户关注消息，点赞消息，回复消息，回帖消息
     *
     */
    private String type;
}
