package com.jhc.DTO;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 11:05
 */
@Data
public class CmsLeaveAddDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 请假人姓名
     */
    private String userName;

    /**
     * 请假人编号
     */
    private String userNumber;

    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 会议id
     */
    private Long meetingId;

    /**
     * 请假理由
     */
    private String reason;

    /**
     * 请假状态 0审核中 1已通过 2未通过
     */
    private Integer status;
}
