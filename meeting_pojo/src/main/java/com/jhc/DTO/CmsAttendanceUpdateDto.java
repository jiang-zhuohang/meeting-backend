package com.jhc.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author jiangzhuohang
 * @create 2022/4/2 10:19
 */
@Data
public class CmsAttendanceUpdateDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userNumber;

    /**
     * 用户姓名
     */
    @ApiModelProperty("用户姓名")
    private String userName;

    /**
     * 会议id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("会议id")
    private Long meetingId;

    /**
     * 会议名称
     */
    @ApiModelProperty("会议名称")
    private String meetingName;

    /**
     * 是否请假 0否 1请假
     */
    @ApiModelProperty("是否请假 0否 1请假")
    private Integer isLeave;

    /**
     * 签到状态 0未签到 1已签到 2迟到
     */
    @ApiModelProperty("签到状态 0未签到 1已签到 2迟到")
    private Integer signInStatus;

    /**
     * 签到方式 1人脸  2手动
     */
    @ApiModelProperty("签到方式 1人脸  2手动")
    private Integer signInMethod;

    /**
     * 签到时间
     */
    @ApiModelProperty("签到时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime signInTime;

    /**
     * 签退状态 0未签退 1已签退 3早退
     */
    @ApiModelProperty("签退状态 0未签退 1已签退 3早退")
    private Integer signOutStatus;

    /**
     *  签退方式 1人脸  2手动
     */
    @ApiModelProperty("签退方式 1人脸  2手动")
    private Integer signOutMethod;

    /**
     * 签退时间
     */
    @ApiModelProperty("签退时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime signOutTime;
}
