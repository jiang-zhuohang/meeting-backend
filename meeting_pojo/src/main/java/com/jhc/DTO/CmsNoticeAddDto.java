package com.jhc.DTO;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author jiangzhuohang
 */
@Data
@ApiModel("CmsNoticeAddDto :: 公告添加")
public class CmsNoticeAddDto implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建用户
     */
    private String createUser;

    /**
     * 状态 0草稿 1发布
     */
    @ApiModelProperty("状态 0草稿 1发布")
    private Integer status;

}