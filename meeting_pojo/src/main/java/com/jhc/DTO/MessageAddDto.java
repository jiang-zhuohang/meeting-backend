package com.jhc.DTO;

import lombok.Data;

@Data
public class MessageAddDto {
    private static final long serialVersionUID=1L;

    /**
     * 发送端编号
     */
    private String sendNumber;

    /**
     * 发送者姓名
     */
    private String sendName;

    /**
     * 接收端编号
     */
    private String acceptNumber;

    /**
     * 内容
     */
    private String content;

    /**
     * 类型  （用户私信   系统通知   被评论回复等消息通知）
     */
    private String type;
}
