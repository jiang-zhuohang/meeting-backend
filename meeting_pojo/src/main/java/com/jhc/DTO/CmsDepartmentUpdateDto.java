package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author jiangzhuohang
 * @create 2022/4/5 23:37
 */
@Data
public class CmsDepartmentUpdateDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;


    /**
     * 部门名称
     */
    private String name;

    /**
     * 部门编号
     */
    private String number;


    /**
     * 负责人编号
     */
    private String userNumber;

    /**
     * 负责人姓名
     */
    private String userName;
}
