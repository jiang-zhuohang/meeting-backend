package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author jiangzhuohang
 * @create 2022/4/6 11:07
 */
@Data
public class CmsLeaveUpdateDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;


    /**
     * 请假人姓名
     */
    private String userName;

    /**
     * 请假人编号
     */
    private String userNumber;

    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 会议id
     */
    private Long meetingId;

    /**
     * 请假理由
     */
    private String reason;

    /**
     * 请假状态
     */
    private Integer status;
}
