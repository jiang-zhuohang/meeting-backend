package com.jhc.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author jiangzhuohang
 * @create 2022/3/19 1:11
 */
@Data
public class CmsMeetingUpdateDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 会议名称
     */
    private String name;

    /**
     * 会议描述
     */
    private String describe;

    /**
     * 发起人编号
     */
    private String userNumber;

    /**
     * 发起人姓名
     */
    private String userName;

    /**
     * 会议成员编号
     */
    private List<String> memberNumber;


    /**
     * 会议类型 0线上 1线下
     */
    private Integer type;

    /**
     * 会议室编号
     */
    private String roomNumber;


    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime endTime;

    /**
     * 签到时间范围
     */
    private Integer timeFrame;

    /**
     * 会议状态 0审核中 1已通过 2未通过 3已开始 4已结束
     */
    private Integer meetingStatus;
}
