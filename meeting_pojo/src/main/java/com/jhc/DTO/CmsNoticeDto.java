package com.jhc.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.DO.BasePage;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 */
@Data
public class CmsNoticeDto extends BasePage {

    /**
     * 标题
     */
    private String title;

}
