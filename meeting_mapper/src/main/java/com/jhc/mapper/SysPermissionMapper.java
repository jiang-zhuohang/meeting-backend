package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DO.SysPermission;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * cms_permission  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-07-02
 */
@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

}
