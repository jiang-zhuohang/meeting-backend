package com.jhc.mapper;

import com.jhc.DO.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 签到表 Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
