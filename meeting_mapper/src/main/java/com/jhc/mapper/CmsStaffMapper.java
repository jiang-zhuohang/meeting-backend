package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsStaff;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DTO.CmsStaffDto;
import com.jhc.VO.CmsStaffVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@Mapper
public interface CmsStaffMapper extends BaseMapper<CmsStaff> {

    IPage<CmsStaffVo> getStaffPageData(Page page, @Param("dto") CmsStaffDto pageVo);
    
}
