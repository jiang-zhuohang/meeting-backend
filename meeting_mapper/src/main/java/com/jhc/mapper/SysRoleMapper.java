package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DO.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * cms_role  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2021-07-02
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    SysRole selectPermission(Long number);
}
