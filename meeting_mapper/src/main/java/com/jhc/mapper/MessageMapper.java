package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DTO.MessageDto;
import com.jhc.VO.MessageVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 消息表 Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-07
 */
public interface MessageMapper extends BaseMapper<Message> {

    IPage<MessageVO> getPageData(Page page, @Param("dto") MessageDto dto);

}
