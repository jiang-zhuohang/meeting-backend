package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsLeader;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DTO.CmsLeaderDto;
import com.jhc.VO.CmsLeaderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@Mapper
public interface CmsLeaderMapper extends BaseMapper<CmsLeader> {
    IPage<CmsLeaderVo> getLeaderPageData(Page page, @Param("dto") CmsLeaderDto pageVo);
}
