package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsDepartment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DTO.CmsDepartmentDto;
import com.jhc.VO.CmsDepartmentVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
public interface CmsDepartmentMapper extends BaseMapper<CmsDepartment> {

    IPage<CmsDepartmentVo> getDepartmentPageData(Page page,@Param("dto") CmsDepartmentDto pageVo);
}
