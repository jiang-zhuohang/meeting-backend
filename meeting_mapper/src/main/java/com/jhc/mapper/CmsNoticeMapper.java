package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsNotice;
import com.jhc.DO.CmsNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DTO.CmsNoticeDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 公告 Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-01-08
 */
@Mapper
public interface CmsNoticeMapper extends BaseMapper<CmsNotice> {

    IPage<CmsNotice> getCmsNoticePage(Page page, @Param("dto") CmsNoticeDto dto);

}
