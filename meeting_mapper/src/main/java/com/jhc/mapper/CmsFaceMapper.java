package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsFace;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.VO.FaceInfoVo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-06-15
 */
public interface CmsFaceMapper extends BaseMapper<CmsFace> {

    IPage<FaceInfoVo> getFacePageData(Page page);
}
