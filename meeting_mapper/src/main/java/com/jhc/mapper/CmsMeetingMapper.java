package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsMeeting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DO.CmsMeeting;
import com.jhc.DTO.CmsMeetingDto;
import com.jhc.VO.CmsMeetingVo;
import com.jhc.VO.JoinMeetingVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-03-18
 */
@Mapper
public interface CmsMeetingMapper extends BaseMapper<CmsMeeting> {
    IPage<CmsMeetingVo> getCmsMeetingPage(Page page, @Param("dto") CmsMeetingDto dto);

    List<CmsMeeting> getUserJoinMeetingList(String userNumber);

    List<JoinMeetingVo> getUserJoinMeetingListHistory(String userNumber);
}
