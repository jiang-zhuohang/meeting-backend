package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsLeave;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DTO.CmsLeaveDto;
import com.jhc.VO.CmsLeaveVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
public interface CmsLeaveMapper extends BaseMapper<CmsLeave> {

    IPage<CmsLeaveVo> getLeavePageData(Page page,@Param("dto") CmsLeaveDto pageVo);
}
