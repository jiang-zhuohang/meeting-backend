package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsAttendance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DTO.CmsAttendanceDto;
import com.jhc.VO.CmsAttendanceVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-02
 */
public interface CmsAttendanceMapper extends BaseMapper<CmsAttendance> {

    IPage<CmsAttendanceVo> getCmsAttendancePage(Page page,@Param("dto") CmsAttendanceDto dto);
}
