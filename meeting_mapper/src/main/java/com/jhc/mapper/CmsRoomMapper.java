package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.DO.CmsRoom;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.DTO.CmsRoomDto;
import com.jhc.VO.CmsRoomVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jiangzhuohang
 * @since 2022-04-06
 */
public interface CmsRoomMapper extends BaseMapper<CmsRoom> {

    IPage<CmsRoomVo> getRoomPageData(Page page,@Param("dto") CmsRoomDto pageVo);
}
